# 3D Viewer Guide

This guide will help you to get the most out of the 3D Viewer.

## Features

Models --> Cuneiform Tablets

- `Rotate` Will allow 3D model to rotate using the mouse or touch(for touchscreens) movement.
- `Move` Model can be moved from its place.
- `Zoom In` With this, view can be zoomed in for an enlarged view of the model.
- `Zoom Out` Zoom out for a less enlarged view of the model.
- `Undo` The last rotation or movement is undone.
- `Reset` All rotations and movements are undone, reseting the model to its starting position.
- `Lighting` The direction of the virtual lighting effect can be moved using the mouse.
- `Fullscreen` Sets the height and width of the 3D Viewer to the full screen view port.

  Note: *All features are accessed from the left side of the 3D Viewer using the associated icons.*
  
## Run Locally

After installing the framework locally:

Activate cdli_env

```bash
  source cdli_env/bin/activate
```

Start server

```bash
  ./framework/dev/cdlidev.py up
```

Open sample link

```bash
  http://localhost:2354/3dviewer/P215518
```

## Screenshot

![App Screenshot](3Dviewershot.png)

## Sample cuneiform tablets for testing

| CDLI Tablet             | URL                                                                |
| ----------------- | ------------------------------------------------------------------ |
| P215518 | [link](http://localhost:2354/3dviewer/P215518) |

## Documentation
[Cakephp documentation](https://book.cakephp.org/4/en/index.html)

[Threejs documentation](https://threejs.org/docs/index.html#manual/en/introduction/Creating-a-scene)

## File Structure

```
--3Dviewer--
│   
│       
└───Cake
│   │   
│   │  
│   └───Config
│   |   │     routes.php (3D viewer page url routes are set from here.)
│   |
|   └───src
|   |    |
|   |    |
|   |    └───Controller
|   |        |    ThreedviewerController.php (Controller for managing all methods according to url input.)
|   |
|   └───templates
|        |
|        └───Threedviewer
|            |    Index.php (Page to display 3Dviewer.)                                   
|   
└───Webroot
    |
    └───assets
    |   |
    |   └───js
    |       |
    |       └───3dviewer
    |           |   InteractiveViewer.js (3dviewer is rendered from here)
    |           |   PLYLoader.js
    |           |   Arcball.js
    |           |   Detector.js
    |           |   Three.min.js
    |           |   jquery-3.1.0.min.js
    |
    └───images
    |   |
    |   └───3dviewericons (All the icons related to 3Dviewer are stored here.)
    |
    └───dl
        |
        └───vcmodels (3D data according to CDLI_No is fetched from here.)
            Note : For testing in local system this folder is mounted with 'vcmodels' of 'dl' in 'stubs' of 'dev' folder.
```

## Contributing

Want to contribute? Great!

3Dviewer uses Threejs library for fast and easy rendering of 3D models.

Please Go through **Documentation** and **File Structure** given above.

Please adhere to this project's `code of conduct`.

## Tech Stack

**3D Geometry input:** Cuneiform tablet 3D model in `.ply` format.

**Texture and color input:** Cuneiform tablet texture image in `.jpg` format.