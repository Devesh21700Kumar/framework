import unittest
import os

# These are the installation tests for ngnix sever
# and phpmyadmin.
class RunScssLintTests(unittest.TestCase):

    def test_combine_lint_of_scss_files(self):
        self.maxDiff = None
        # This files lints all the scss files
        # ./dev/assets/sass/**.scss see .scss-lint.yml
        # to know more.
        command_scsslint = os.popen('scss-lint --config .scss-lint.yml')
        command_scsslint_result = command_scsslint.read()
        command_scsslint_result
        # For better linting display.
        if command_scsslint_result=='':
            result = 'passed!'
        else:
            result = 'failed!'
        command_scsslint.close()
        print('Linting ./dev/assets/sass/**.scss...... '+result)
        self.assertEqual(command_scsslint_result, '')
        
if __name__ == '__main__':
    unittest.main()
