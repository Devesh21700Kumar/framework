<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Submission $submission
 * @var \Cake\Collection\CollectionInterface|string[] $sections
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Submissions'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="submissions form content">
            <?= $this->Form->create($submission) ?>
            <fieldset>
                <legend><?= __('Add Submission') ?></legend>
                <?php
                    echo $this->Form->control('locale');
                    echo $this->Form->control('context_id');
                    echo $this->Form->control('section_id', ['options' => $sections, 'empty' => true]);
                    echo $this->Form->control('current_publication_id');
                    echo $this->Form->control('date_last_activity', ['empty' => true]);
                    echo $this->Form->control('date_submitted', ['empty' => true]);
                    echo $this->Form->control('last_modified', ['empty' => true]);
                    echo $this->Form->control('stage_id');
                    echo $this->Form->control('status');
                    echo $this->Form->control('submission_progress');
                    echo $this->Form->control('work_type');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
