<div>
   <?= $this->Html->css('3dviewer'); ?>
   <?= $this->Html->script('3dviewer/jquery-3.1.0.min'); ?>
   <?= $this->Html->script('3dviewer/three.min'); ?>
   <?= $this->Html->script('3dviewer/PLYLoader'); ?>
   <?= $this->Html->script('3dviewer/Detector'); ?>
   <?= $this->Html->script('3dviewer/Arcball'); ?>
   <?= $this->Html->script('3dviewer/InteractiveViewer'); ?>   
</div>
<?php
$envelope_ext = '_e';
if (strpos($CDLI_NO, $envelope_ext) !== false) {
    $CDLI_NO_ext = str_replace("_e","",$CDLI_NO);
    $CDLI_NO_ext .= "-Envelope";
}
else {
    $CDLI_NO_ext = $CDLI_NO;
}
$this->assign('title', $artifact->designation . ' (' . $CDLI_NO . ')'); 
?>
    <h1 class="display-3 header-txt" style="text-align: left;"><?= h($artifact->designation) ?> (<?= h($CDLI_NO_ext) ?>)</h1>
    <h2 class="my-4 artifact-desc " style="text-align: left;">
        <?php if (!empty($artifact->genres)): ?>
            <?= h($artifact->genres[0]->genre) ?>,
        <?php endif; ?>
        <?php if (!empty($artifact->artifact_type)): ?>
            <?= h($artifact->artifact_type->artifact_type) ?>,
        <?php endif; ?>
        <?php if (!empty($artifact->provenience)): ?>
            <?= h($artifact->provenience->provenience) ?>
        <?php endif; ?>
        <?php if (!empty($artifact->period)): ?>
            in <?= h($artifact->period->period) ?>
        <?php endif; ?>
        <?php if (!empty($artifact->collections)): ?>
            and kept at <?= h($artifact->collections[0]->collection) ?>
        <?php endif; ?>
    </h2>
<div class="canvas">
   <div id="help">
      <div class="help-header">      
         Control Guide
         <div class="help-icon">
              <img id="help-close" src="../images/3dviewericons/closeicon.svg" alt="Help close icon">
         </div>
      </div>
      <div class="help-body animate">
         <div class="row">
            <div class="col">
                <?php echo $this->Html->image('../../images/3dviewericons/rotate.svg', ['alt' => 'Rotate icon']); ?>
               <span class="hads">Rotate</span><br><span class="conts"> Drag model to rotate in 360 degrees. <br> ( R )</span>
            </div>
            <div class="col">
                <?php echo $this->Html->image('../../images/3dviewericons/zoomin.svg', ['alt' => 'Zoom IN icon']); ?>
               <span class="hads">Zoom In</span><br><span class="conts"> Makes model larger on every click. <br> ( + )</span>
            </div>
            <div class="col">
                <?php echo $this->Html->image('../../images/3dviewericons/undo.svg', ['alt' => 'Undo icon']); ?>
               <span class="hads">Undo</span><br><span class="conts"> Undo last interaction. <br> ( Z )</span>
            </div>
               <div class="col">
                   <?php echo $this->Html->image('../../images/3dviewericons/light.svg', ['alt' => 'Light icon']); ?>
            <span class="hads">Light</span><br><span class="conts"> Drag mouse to change light direction. <br> ( L )</span>
            </div>
         </div>
         <div class="row">
            <div class="col">
                <?php echo $this->Html->image('../../images/3dviewericons/move.svg', ['alt' => 'Move icon']); ?>
               <span class="hads">Move</span><br><span class="conts"> Drag model to change position.<br> ( M )</span>
            </div>
            <div class="col">
                <?php echo $this->Html->image('../../images/3dviewericons/zoomout.svg', ['alt' => 'Zoom out icon']); ?>
            <span class="hads">Zoom Out</span><br><span class="conts"> Makes model smaller on every click. <br> ( - )</span>
            </div>
            <div class="col">
                <?php echo $this->Html->image('../../images/3dviewericons/reset.svg', ['alt' => 'Reset icon']); ?>
            <span class="hads">Reset</span><br><span class="conts"> Return model to original view setting.</span>
            </div>
            <div class="col">
            <?php echo $this->Html->image('../../images/3dviewericons/fullscreen.svg', ['alt' => 'Fullscreen icon']); ?>
               <span class="hads">Full Screen</span><br><span class="conts"> Makes viewer cover full screen. <br> (F, Esc to exit)</span>
            </div>
         </div>
         
      </div>           
   </div>
       <div class="threeDcontainer" id="canvas" style="border: none; filter: blur(0);"></div>
       <div id="progress">
           <div id="progressGif"><img src="../images/3dviewericons/progress.gif"></div>
           <div id="message">Loading 3D model data ...</div>     
       </div>

</div>
<p class="infoText">3D model view of CDLI <a id="cdlilink"><?php echo "$CDLI_NO"; ?></a>
   <?php
      $envelope_ext = '_e';
        if (strpos($CDLI_NO, $envelope_ext) == false) 
        {
            $folderID = $CDLI_NO.$envelope_ext;
            $model = WWW_ROOT . 'dl' . DS . 'vcmodels' . DS . $folderID . DS . $CDLI_NO . $envelope_ext .'.ply';
            $texture = WWW_ROOT . 'dl' . DS . 'vcmodels' . DS . $folderID . DS . $CDLI_NO . $envelope_ext .'.jpg'; 
            if (file_exists($model) && file_exists($texture))
            {
                if ($env_prms == 1)
                {
                    $CDLI_NO_env = $CDLI_NO . '_e';
                    echo ", <a href=/3dviewer/$CDLI_NO_env >View Envelope</a>";
                }
            }
        } 
        elseif (strpos($CDLI_NO, $envelope_ext) !== false)
        {
            if ($tab_prms == 1)
            {
                $CDLI_NO_tab = str_replace("_e","",$CDLI_NO);
                echo ", <a href=/3dviewer/$CDLI_NO_tab >View Tablet</a>";
            }
        }
   ?>
</p>
<?php
    $clctn = h($artifact->collections[0]->collection);
    if ($clctn == 'World Museum Liverpool, Liverpool, UK') {
        echo "<p class=infoText>Courtesy of National Museums Liverpool (World Museum)</p>";
    }
?>
       <div class="artifact-summary font-weight-light" style="text-align: left;">
        <h2 class="font-weight-light">Summary</h2>
        <div class="mt-4 d-block d-md-flex justify-content-between">
            <div>
                <p>Musuem Collection</p>
                <?php foreach ($artifact->collections as $collection): ?>
                    <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
                <?php endforeach; ?>

                <p>Period</p>
                <?php if (!empty($artifact->period)): ?>
                    <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
                <?php endif; ?>

                <p>Provenience</p>
                <?php if (!empty($artifact->provenience)): ?>
                    <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
                <?php endif; ?>

                <p>Artifact Type</p>
                <?php if (!empty($artifact->artifact_type)): ?>
                    <?= $this->Html->link($artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
                <?php endif; ?>
            </div>
            <div>
                <p>Material</p>
                <?php foreach ($artifact->materials as $material): ?>
                    <?= $this->Html->link($material->material, ['controller' => 'Materials', 'action' => 'view', $material->id]) ?>
                <?php endforeach; ?>

                <p>Genre / Subgenre</p>
                <?php foreach ($artifact->genres as $genre): ?>
                    <?= $this->Html->link($genre->genre, ['controller' => 'Genres', 'action' => 'view', $genre->id]) ?>
                <?php endforeach; ?>

                <p>Language</p>
                <?php foreach ($artifact->languages as $language): ?>
                    <?= $this->Html->link($language->language, ['controller' => 'Languages', 'action' => 'view', $language->id]) ?>
                <?php endforeach; ?>                           
            </div>
        </div>
    </div>
<p class="infoText">3D Viewer Designed by <a href="http://virtualcuneiform.org/">The Virtual Cuneiform Tablet Reconstruction Project</a></p>