<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <?= $this->Html->link(
            '<span class="fa fa-chevron-left"></span> ' . __('Back to artifact'),
            ['action' => 'view', $artifact->id],
            ['escapeTitle' => false]
        ) ?>
    </div>

    <?php foreach ($updates as $update): ?>
        <div class="col-lg-12 boxed">
            <h2><?= $this->element('updateEventHeader', ['update_event' => $update->update_event]) ?></h2>

            <?php if (!empty($update->update_event->event_comments)): ?>
                <p><?= h($update->update_event->event_comments) ?></p>
            <?php endif; ?>

            <?= $this->element('artifactUpdateDiff', ['artifact_update' => $update]) ?>
        </div>
    <?php endforeach; ?>

    <div class="col-lg-12 boxed">
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
