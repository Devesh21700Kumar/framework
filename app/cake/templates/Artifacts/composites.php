<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */
?>
<h1 class="display-3 header-txt text-left">Composites List</h1>


<p class="text-justify page-summary-text mt-4">
    Composite texts are scholarly re-created "complete" versions of texts based on the various witnesses ancient Mesopotamians have written in the past and which survive to this day. These virtual complete versions of compositions help us understand the memorized, oral, or copied text which was at the origin of the written ones. They also help us reconstruct whole stories and lists since many witnesses are incomplete, because they are broken or because the scribe only wrote a section of the whole composition.
</p>
<?= $this->Html->link('Royal', ['action' => 'composites','Royal'], ['class' => 'btn btn-action']) ?>
<?= $this->Html->link('Lexical', ['action' => 'composites','Lexical'], ['class' => 'btn btn-action']) ?>
<?= $this->Html->link('Literature', ['action' => 'composites','Literary'], ['class' => 'btn btn-action']) ?>
<?= $this->Html->link('Scientific', ['action' => 'composites', 'Scientific'], ['class' => 'btn btn-action']) ?>

<?php
    if(isset($compositetype)){
?>

<table class="table-bootstrap my-3 mx-0">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('composite_no','Composite Number') ?></th>
            <th scope="col"><?= $this->Paginator->sort('designation','Composite Designation') ?></th>
            <th scope="col"><?= h('Witnesses') ?></th>
            <th scope="col"><?= h('Tagged Witness') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifacts as $artifacts):
            //if(strpos($artifacts->designation, 'composite') !== false){
               
            ?>
        <tr>
            <td><?= $this->Html->link($artifacts->composite_no,['action' => 'compositesScore',$artifacts->composite_no]);?></td>
            <td><?= h($artifacts->designation) ?></td>
            <td><?php echo count($artifacts->witnesses);   ?> </td>
            <td>
                <?php
                    $tagged_witness_count =0;
                    for ($i = 1;$i < count($artifacts->witnesses);$i++) {
                        if ((!is_null($artifacts->witnesses[$i]->inscription))  && ($artifacts->witnesses[$i]->composite_no == '') ) {
                            $tagged_witness_count = $tagged_witness_count+1;
                        }
                    }
                    echo $tagged_witness_count;
                ?>
            </td>
        </tr>
        <?php 
           // }
    endforeach; ?>
    </tbody>
</table>

<?php
        echo $this->element('Paginator');
    }
?>
