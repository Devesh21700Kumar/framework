<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author[]|\Cake\Collection\CollectionInterface $authors
 */
?>


<h1 class="display-3 header-txt text-left">Authors Index</h1>
<?php foreach (range('A', 'Z') as $char): ?>
    <?= $this->Html->link($char, ['action' => 'view', '?' => ['letter' => $char]], ['class' => 'btn btn-action']) ?>
<?php endforeach ?>

<table class="table-bootstrap my-3 mx-0">
    <thead>
        <tr>
            <th scope="col">Author Name</th>
            <th scope="col">Institution</th>
            <th scope="col">Email</th>
            <th scope="col">ORCID ID</th>
            <th scope="col">Deceased</th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($authors as $author): ?>
        <tr>
            <td><?= $this->Html->link($author->author, ['prefix' => false, 'action' => 'view', $author->id]) ?></td>
            <td><?= h($author->institution) ?></td>
            <td><?= h($author->email) ?></td>
            <td><?= h($author->orcid_id) ?></td>
            <td><?= ($author->deceased)==1? h('Yes') : h('No') ?></td>
            <td>
            <?php if ($access_granted): ?>
                <?= $this->Html->image("/images/edit.svg", [
                        "alt" => "Edit",
                        'url' => ['prefix' => 'Admin', 'action' => 'edit', $author->id],
                        'title' => 'Edit'
                    ]);
                    ?>
            <?php endif ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

