<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Genre $genre
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($genre) ?>
            <legend class="capital-heading"><?= __('Add Genre') ?></legend>
            <?php
                echo $this->Form->control('genre');
                echo $this->Form->control('parent_id', ['options' => $parentGenres, 'empty' => true]);
                echo $this->Form->control('genre_comments');
                echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
