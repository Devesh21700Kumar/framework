<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Submission $submission
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Submission'), ['action' => 'edit', $submission->submission_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Submission'), ['action' => 'delete', $submission->submission_id], ['confirm' => __('Are you sure you want to delete # {0}?', $submission->submission_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Submissions'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Submission'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="submissions view content">
            <h3><?= h($submission->submission_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Locale') ?></th>
                    <td><?= h($submission->locale) ?></td>
                </tr>
                <tr>
                    <th><?= __('Section') ?></th>
                    <td><?= $submission->has('section') ? $this->Html->link($submission->section->section_id, ['controller' => 'Sections', 'action' => 'view', $submission->section->section_id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Submission Id') ?></th>
                    <td><?= $this->Number->format($submission->submission_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Context Id') ?></th>
                    <td><?= $this->Number->format($submission->context_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Current Publication Id') ?></th>
                    <td><?= $this->Number->format($submission->current_publication_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Stage Id') ?></th>
                    <td><?= $this->Number->format($submission->stage_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Status') ?></th>
                    <td><?= $this->Number->format($submission->status) ?></td>
                </tr>
                <tr>
                    <th><?= __('Submission Progress') ?></th>
                    <td><?= $this->Number->format($submission->submission_progress) ?></td>
                </tr>
                <tr>
                    <th><?= __('Work Type') ?></th>
                    <td><?= $this->Number->format($submission->work_type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Date Last Activity') ?></th>
                    <td><?= h($submission->date_last_activity) ?></td>
                </tr>
                <tr>
                    <th><?= __('Date Submitted') ?></th>
                    <td><?= h($submission->date_submitted) ?></td>
                </tr>
                <tr>
                    <th><?= __('Last Modified') ?></th>
                    <td><?= h($submission->last_modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Review Assignments') ?></h4>
                <?php if (!empty($submission->review_assignments)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Review Id') ?></th>
                            <th><?= __('Submission Id') ?></th>
                            <th><?= __('Reviewer Id') ?></th>
                            <th><?= __('Competing Interests') ?></th>
                            <th><?= __('Recommendation') ?></th>
                            <th><?= __('Date Assigned') ?></th>
                            <th><?= __('Date Notified') ?></th>
                            <th><?= __('Date Confirmed') ?></th>
                            <th><?= __('Date Completed') ?></th>
                            <th><?= __('Date Acknowledged') ?></th>
                            <th><?= __('Date Due') ?></th>
                            <th><?= __('Date Response Due') ?></th>
                            <th><?= __('Last Modified') ?></th>
                            <th><?= __('Reminder Was Automatic') ?></th>
                            <th><?= __('Declined') ?></th>
                            <th><?= __('Cancelled') ?></th>
                            <th><?= __('Reviewer File Id') ?></th>
                            <th><?= __('Date Rated') ?></th>
                            <th><?= __('Date Reminded') ?></th>
                            <th><?= __('Quality') ?></th>
                            <th><?= __('Review Round Id') ?></th>
                            <th><?= __('Stage Id') ?></th>
                            <th><?= __('Review Method') ?></th>
                            <th><?= __('Round') ?></th>
                            <th><?= __('Step') ?></th>
                            <th><?= __('Review Form Id') ?></th>
                            <th><?= __('Unconsidered') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($submission->review_assignments as $reviewAssignments) : ?>
                        <tr>
                            <td><?= h($reviewAssignments->review_id) ?></td>
                            <td><?= h($reviewAssignments->submission_id) ?></td>
                            <td><?= h($reviewAssignments->reviewer_id) ?></td>
                            <td><?= h($reviewAssignments->competing_interests) ?></td>
                            <td><?= h($reviewAssignments->recommendation) ?></td>
                            <td><?= h($reviewAssignments->date_assigned) ?></td>
                            <td><?= h($reviewAssignments->date_notified) ?></td>
                            <td><?= h($reviewAssignments->date_confirmed) ?></td>
                            <td><?= h($reviewAssignments->date_completed) ?></td>
                            <td><?= h($reviewAssignments->date_acknowledged) ?></td>
                            <td><?= h($reviewAssignments->date_due) ?></td>
                            <td><?= h($reviewAssignments->date_response_due) ?></td>
                            <td><?= h($reviewAssignments->last_modified) ?></td>
                            <td><?= h($reviewAssignments->reminder_was_automatic) ?></td>
                            <td><?= h($reviewAssignments->declined) ?></td>
                            <td><?= h($reviewAssignments->cancelled) ?></td>
                            <td><?= h($reviewAssignments->reviewer_file_id) ?></td>
                            <td><?= h($reviewAssignments->date_rated) ?></td>
                            <td><?= h($reviewAssignments->date_reminded) ?></td>
                            <td><?= h($reviewAssignments->quality) ?></td>
                            <td><?= h($reviewAssignments->review_round_id) ?></td>
                            <td><?= h($reviewAssignments->stage_id) ?></td>
                            <td><?= h($reviewAssignments->review_method) ?></td>
                            <td><?= h($reviewAssignments->round) ?></td>
                            <td><?= h($reviewAssignments->step) ?></td>
                            <td><?= h($reviewAssignments->review_form_id) ?></td>
                            <td><?= h($reviewAssignments->unconsidered) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'ReviewAssignments', 'action' => 'view', $reviewAssignments->review_id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'ReviewAssignments', 'action' => 'edit', $reviewAssignments->review_id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReviewAssignments', 'action' => 'delete', $reviewAssignments->review_id], ['confirm' => __('Are you sure you want to delete # {0}?', $reviewAssignments->review_id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Queries') ?></h4>
                <?php if (!empty($submission->queries)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Query Id') ?></th>
                            <th><?= __('Assoc Type') ?></th>
                            <th><?= __('Assoc Id') ?></th>
                            <th><?= __('Stage Id') ?></th>
                            <th><?= __('Seq') ?></th>
                            <th><?= __('Date Posted') ?></th>
                            <th><?= __('Date Modified') ?></th>
                            <th><?= __('Closed') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($submission->queries as $queries) : ?>
                        <tr>
                            <td><?= h($queries->query_id) ?></td>
                            <td><?= h($queries->assoc_type) ?></td>
                            <td><?= h($queries->assoc_id) ?></td>
                            <td><?= h($queries->stage_id) ?></td>
                            <td><?= h($queries->seq) ?></td>
                            <td><?= h($queries->date_posted) ?></td>
                            <td><?= h($queries->date_modified) ?></td>
                            <td><?= h($queries->closed) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Queries', 'action' => 'view', $queries->query_id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Queries', 'action' => 'edit', $queries->query_id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Queries', 'action' => 'delete', $queries->query_id], ['confirm' => __('Are you sure you want to delete # {0}?', $queries->query_id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
