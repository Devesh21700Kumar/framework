<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation $abbreviation
 */
?>
<div class="row justify-content-md-center">
 
    <div class="col-lg-7 boxed">
        <div class="abbreviations form content">
            <?= $this->Form->create($abbreviation)?>
            <fieldset>
                <legend><?= __('Add Abbreviation') ?></legend>
                <?php
                    echo $this->Form->control('abbreviation');
                    echo $this->Form->control('fullform');
                    echo $this->Form->control('publication_id');
                    echo $this->Form->control('type');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>

</div>
