<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Material $material
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($material) ?>
            <legend class="capital-heading"><?= __('Edit Material') ?></legend>
            <?php
                echo $this->Form->control('material');
                echo $this->Form->control('parent_id', ['options' => $parentMaterials, 'empty' => true]);
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>  
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $material->id],
                ['class' => 'btn btn-danger float-right'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $material->id)]
            )
        ?>
        </div>

    </div>

</div>
