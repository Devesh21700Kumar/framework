<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dynasty $dynasty
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($dynasty) ?>
            <legend class="capital-heading"><?= __('Edit Dynasty') ?></legend>
            <?php
                echo $this->Form->control('polity');
                echo $this->Form->control('dynasty');
                echo $this->Form->control('sequence');
                echo $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dynasty->id],
                ['class' => 'btn btn-danger float-right'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dynasty->id)]
            )
        ?>
        </div>

    </div>

</div>
