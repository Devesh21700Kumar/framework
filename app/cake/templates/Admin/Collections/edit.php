<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection $collection
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($collection) ?>
            <legend class="capital-heading"><?= __('Edit Collection') ?></legend>
            <?php
                echo $this->Form->control('collection', ['class' => 'form-control w-100 mb-3', 'label' => 'Collection', 'type' => 'text', 'maxLength' => 255]);
                echo $this->Form->control('geo_coordinates', ['class' => 'form-control w-100 mb-3', 'label' => 'Geo-Coordiantes', 'type' => 'text', 'maxLength' => 50]);
                echo $this->Form->control('slug', ['class' => 'form-control w-100 mb-3', 'label' => 'Slug', 'type' => 'text', 'maxLength' => 50]);
                echo $this->Form->control('is_private', ['class' => 'mb-3 mr-1', 'label' => 'Is Private', 'type' => 'checkbox']);
                echo $this->Form->control('description', ['class' => 'form-control w-100 mb-3', 'label' => 'Description', 'type' => 'textarea', 'maxLength' => 4000]);
            ?>
            <div class="mb-3"></div>

        <div>
        <?= $this->Form->button(__('Save Changes'),['class'=> 'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $collection->id],
                ['class' => 'btn btn-outline-danger'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $collection->id)]
            )
        ?>
        </div>

    </div>

</div>

<!-- Add CKEDITOR to Body Field -->
<script src="/assets/js/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'description', {
	extraPlugins: 'autogrow',
	autoGrow_maxHeight: 800,
	removePlugins: 'resize'
});
</script>

<!-- Show Alert box on reload -->
<script type="text/javascript">
    var submit = document.getElementById("submit");
    var count = false;
    submit.onclick = () => {
        count = true;
    }

    window.addEventListener('beforeunload', function (e) {
    if(count) {
        return false;
    }
    else {
    e.preventDefault(); 
    e.returnValue = '';
    }
    });
</script>