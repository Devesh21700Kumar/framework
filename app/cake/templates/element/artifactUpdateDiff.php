<table class="table">
    <tbody>
        <?php foreach ($artifact_update->getChanged() as $field): ?>
            <?php $value = $artifact_update->get($field); ?>
            <tr>
                <th><?= h($field) ?></th>
                <td>
                    <?php if (is_null($value)): ?>
                        <del>&mdash;</del>
                    <?php else: ?>
                        <ins><?= h($value) ?></ins>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
