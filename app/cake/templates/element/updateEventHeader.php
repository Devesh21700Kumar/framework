<?= $this->Html->link(
    h($update_event->created),
    ['controller' => 'UpdateEvents', 'action' => 'view', $update_event->id],
    ['escape' => false]
) ?>

<?php if (!empty($update_event->creator)): ?>
    by

    <?= $this->Html->link(
        $update_event->creator->author,
        ['controller' => 'Authors', 'action' => 'view', $update_event->creator->id]
    ) ?>
<?php endif; ?>
