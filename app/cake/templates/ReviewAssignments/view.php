<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReviewAssignment $reviewAssignment
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Review Assignment'), ['action' => 'edit', $reviewAssignment->review_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Review Assignment'), ['action' => 'delete', $reviewAssignment->review_id], ['confirm' => __('Are you sure you want to delete # {0}?', $reviewAssignment->review_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Review Assignments'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Review Assignment'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="reviewAssignments view content">
            <h3><?= h($reviewAssignment->review_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Submission') ?></th>
                    <td><?= $reviewAssignment->has('submission') ? $this->Html->link($reviewAssignment->submission->submission_id, ['controller' => 'Submissions', 'action' => 'view', $reviewAssignment->submission->submission_id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Review Round') ?></th>
                    <td><?= $reviewAssignment->has('review_round') ? $this->Html->link($reviewAssignment->review_round->review_round_id, ['controller' => 'ReviewRounds', 'action' => 'view', $reviewAssignment->review_round->review_round_id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Review Id') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->review_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reviewer Id') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->reviewer_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Recommendation') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->recommendation) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reminder Was Automatic') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->reminder_was_automatic) ?></td>
                </tr>
                <tr>
                    <th><?= __('Declined') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->declined) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cancelled') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->cancelled) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reviewer File Id') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->reviewer_file_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Quality') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->quality) ?></td>
                </tr>
                <tr>
                    <th><?= __('Stage Id') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->stage_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Review Method') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->review_method) ?></td>
                </tr>
                <tr>
                    <th><?= __('Round') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->round) ?></td>
                </tr>
                <tr>
                    <th><?= __('Step') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->step) ?></td>
                </tr>
                <tr>
                    <th><?= __('Review Form Id') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->review_form_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Unconsidered') ?></th>
                    <td><?= $this->Number->format($reviewAssignment->unconsidered) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reviewer Name') ?></th>
                    <td> <?= h($reviewAssignment->user_settings[2]->setting_value)." ".h($reviewAssignment->user_settings[1]->setting_value) ?> </td>
                </tr>
                <tr>
                    <th><?= __('Reviewer Name') ?></th>
                    <?= dd($reviewAssignment->user_settings[2]->users_oj->user_id) ?>
                    <td> <?= h($reviewAssignment->user_settings[1]->user_oj->user_id)." ".h($reviewAssignment->user_settings[1]->setting_value) ?> </td>
                </tr>
                <tr>
                    <th><?= __('First Submission Date') ?></th>
                    <td> <?= h($reviewAssignment->submission->date_submitted)?> </td>
                </tr>
                <tr>
                    <th><?= __('Review Date') ?></th>
                    <td> <?= h($reviewAssignment->date_completed)?> </td>
                </tr>
                <tr>
                    <th><?= __('Review') ?></th>
                    <td> <?= h($reviewAssignment->submission_comments[0]->comments)?> </td>
                </tr>
                <tr>
                    <th><?= __('Review') ?></th>
                    <td> <?= h($reviewAssignment->submission->section->section_id)?> </td>
                </tr>
                

            </table>
            <div class="text">
                <strong><?= __('Competing Interests') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($reviewAssignment->competing_interests)); ?>
                </blockquote>
            </div>
        </div>
    </div>
</div>
