<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */

$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2><?= __('Upload') ?></h2>

        <?= $this->Form->create(null, [
            'type' => 'file',
            'url' => ['controller' => 'ArtifactsUpdates', 'action' => 'add']
        ]) ?>
            <?= $this->Form->control('csv', [
                'class' => 'form-control',
                'label' => '',
                'type' => 'file'
            ]) ?>
            <?= $this->Form->submit('Upload', ['class' => 'btn cdli-btn-blue']) ?>
        <?= $this->Form->end(); ?>
    </div>
</div>
