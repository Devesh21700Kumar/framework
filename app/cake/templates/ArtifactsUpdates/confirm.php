<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */

$changedColumns = [];
foreach ($artifactUpdates as $update) {
    foreach ($update->getChanged() as $key) {
        $changedColumns[$key] = true;
    }
}
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2><?= __('Changes') ?></h2>

        <hr>

        <?php if ($canSubmitEdits): ?>
            <?= $this->Html->link(
                __('Confirm upload'),
                ['controller' => 'UpdateEvents', 'action' => 'add'],
                ['class' => 'btn cdli-btn-blue']
                ) ?>
        <?php endif; ?>
        <?= $this->Form->postLink(
            __('Cancel'),
            ['controller' => 'ArtifactsUpdates', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light', 'confirm' => __('Are you sure?')]
        ) ?>
    </div>

    <div class="col-lg-12 boxed">
        <div style="overflow-x: auto;">
            <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                <thead>
                    <tr>
                        <th>artifact</th>
                        <?php foreach ($changedColumns as $key => $value): ?>
                            <th><?= h($key) ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($artifactUpdates as $update): ?>
                        <?php $old = empty($update->artifact) ? [] : $update->artifact->getFlatData(); ?>
                        <tr>
                            <td rowspan="2">
                                <?= h($update->getDesignation()) ?>
                            </td>
                            <?php foreach ($changedColumns as $field => $value): ?>
                                <?php if ($field != 'artifact'): ?>
                                    <?php if ($update->has($field)): ?>
                                        <?php $value = $update->get($field); ?>
                                        <?php if (is_null($value)): ?>
                                            <td rowspan="2"><del><?= $old[$field] ?></del></td>
                                        <?php elseif (!array_key_exists($field, $old) || is_null($old[$field])): ?>
                                            <td rowspan="2"><ins><?= h($value) ?></ins></td>
                                        <?php else: ?>
                                            <td><ins><?= h($value) ?></ins></td>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <td rowspan="2"></td>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>
                        <tr>
                            <?php foreach ($changedColumns as $field => $value): ?>
                                <?php if ($field != 'artifact' && $update->has($field)): ?>
                                    <?php $value = $update->get($field); ?>
                                    <?php if (!is_null($value) && array_key_exists($field, $old) && !is_null($old[$field])): ?>
                                        <td><del><?= $old[$field] ?></del></td>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
