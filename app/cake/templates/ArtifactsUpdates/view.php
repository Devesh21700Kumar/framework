<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2>
            <?php if (empty($artifactsUpdate->artifact_id)): ?>
                <?= h($artifactsUpdate->getDesignation()) ?>
            <?php else: ?>
                <?= $this->Html->link(
                    h($artifactsUpdate->getDesignation()),
                    ['controller' => 'Artifacts', 'action' => 'view', $artifactsUpdate->artifact_id]
                ) ?>
            <?php endif; ?>
        </h2>

        <?php if (!empty($artifactsUpdate->update_event->event_comments)): ?>
            <p><?= h($artifactsUpdate->update_event->event_comments) ?></p>
        <?php endif; ?>
        <p><?= $this->element('updateEventHeader', ['update_event' => $artifactsUpdate->update_event]) ?></p>

        <?php // Only admin should see this but publication_error should only be set on non-approved update events ?>
        <?php if (!empty($artifactsUpdate->publication_error)): ?>
            <details>
                <summary class="text-danger">
                    <span class="fa fa-exclamation-circle" title="Errors"></span>
                    <?= __('Errors') ?>
                </summary>
                <?php $errors = json_decode($artifactsUpdate->publication_error, true); ?>
                <table class="table my-3">
                    <tbody>
                        <?php foreach ($errors as $field => $messages): ?>
                            <?php foreach ($messages as $index => $message): ?>
                                <?php if ($index == 0 && count($messages) > 1): ?>
                                    <th rowspan="<?= count($messages) ?>">
                                <?php else: ?>
                                    <th>
                                <?php endif; ?>
                                    <?= h($field) ?>
                                </th>

                                <td><?= h($message) ?></td>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </details>

            <hr>
        <?php endif; ?>

        <?= $this->element('artifactUpdateDiff', ['artifact_update' => $artifactsUpdate]) ?>
    </div>
</div>
