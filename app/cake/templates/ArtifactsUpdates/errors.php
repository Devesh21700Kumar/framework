<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2><?= __('Encountered errors') ?></h2>

        <ul>
            <?php foreach ($errors as $error): ?>
                <li><?= h($error) ?></li>
            <?php endforeach; ?>
        </ul>

        <?= $this->Form->postLink(
            __('Cancel'),
            ['controller' => 'ArtifactsUpdates', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light']
        ) ?>
    </div>

    <div class="col-lg-12 boxed">
        <?php foreach ($artifactUpdates as $update): ?>
            <?php if ($update->hasErrors()): ?>
                <section>
                    <h3><?= h($update->getDesignation()) ?></h3>

                    <ul>
                        <?php foreach ($update->getErrors() as $field => $errors): ?>
                            <?php foreach ($errors as $error): ?>
                                <li><?= h($field) ?>: <?= h($error) ?></li>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </ul>
                </section>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
