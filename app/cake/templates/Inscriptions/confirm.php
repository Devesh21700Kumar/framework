<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */

use App\Utility\AtfDiffer;
use App\Utility\HtmlDiffOutputBuilder;

$differ = new AtfDiffer(new HtmlDiffOutputBuilder);
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2><?= __('Changes') ?></h2>

        <hr>

        <?php if ($canSubmitEdits): ?>
            <?= $this->Html->link(
                __('Confirm upload'),
                ['controller' => 'UpdateEvents', 'action' => 'add'],
                ['class' => 'btn cdli-btn-blue']
                ) ?>
        <?php endif; ?>
        <?= $this->Form->postLink(
            __('Cancel'),
            ['controller' => 'Inscriptions', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light', 'confirm' => __('Are you sure?')]
        ) ?>
    </div>

    <div class="col-lg-12 boxed">
        <div style="overflow-x: auto;">
            <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                <thead>
                    <tr>
                        <th><?= __('Artifact') ?></th>
                        <th><?= __('Inscription') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($inscriptions as $inscription): ?>
                        <tr>
                            <td><?= h($inscription->artifact->designation) ?></td>
                            <td>
                                <?php if ($inscription->atf == $inscription->artifact->inscription->atf): ?>
                                    <?= __('No changes') ?>
                                <?php else: ?>
                                    <pre><?= $differ->diff($inscription->atf, $inscription->artifact->inscription->atf) ?></pre>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
