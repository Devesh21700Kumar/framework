<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2><?= __('Encountered errors') ?></h2>

        <ul>
            <?php foreach ($errors as $error): ?>
                <li><?= h($error) ?></li>
            <?php endforeach; ?>
        </ul>

        <?= $this->Form->postLink(
            __('Cancel'),
            ['controller' => 'Inscriptions', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light']
        ) ?>
    </div>

    <div class="col-lg-12 boxed">
        <?php foreach ($inscriptions as $inscription): ?>
            <section>
                <h3><?= h($inscription->artifact_id) ?></h3>

                <ul>
                    <?php foreach ($inscription->getErrors() as $field => $errors): ?>
                        <?php foreach ($errors as $error): ?>
                            <li><?= h($field) ?>: <pre><?= h($error) ?></pre></li>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </ul>
            </section>
        <?php endforeach; ?>
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
