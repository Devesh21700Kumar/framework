<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <dl>
            <dt>Description</dt>
            <dd><?= h($updateEvent->event_comments) ?></dd>

            <dt>Type</dt>
            <dd><?= h($updateEvent->update_type) ?></dd>

            <?php if ($updateEvent->has('external_resource')): ?>
                <dt>Project</dt>
                <dd><?= $this->Html->link(
                    h($updateEvent->external_resource->external_resource),
                    ['controller' => 'ExternalResources', 'action' => 'view', $updateEvent->external_resource->id]
                ) ?></dd>
            <?php endif; ?>

            <dt>Creator</dt>
            <dd><?= $this->Html->link(
                h($updateEvent->creator->author),
                ['controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
            ) ?></dd></dd>

            <dt>Created</dt>
            <dd><?= h($updateEvent->created) ?></dd>

            <dt>Status</dt>
            <dd><?= h($updateEvent->status) ?></dd>

            <?php if ($updateEvent->has('reviewer')): ?>
                <dt>Approved by</dt>
                <dd><?= $this->Html->link(
                    h($updateEvent->reviewer->author),
                    ['controller' => 'Authors', 'action' => 'view', $updateEvent->reviewer->id]
                ) ?></dd>
            <?php endif; ?>
        </dl>
    </div>

    <?php
      // - admin and author can delete unless approved
      // - author can edit and submit unless already submitted
      // - reviewer (includes admin) can approve when submitted
    ?>
    <?php if ($updateEvent->status != 'approved' && ($isAdmin || $isReviewer || $isAuthor)): ?>
        <div class="col-lg-7 boxed">
            <?php if ($isReviewer && $updateEvent->status == 'submitted'): ?>
                <?= $this->Form->postLink(
                    __('Approve & apply'),
                    ['controller' => 'UpdateEvents', 'action' => 'approve', $updateEvent->id],
                    ['class' => 'btn cdli-btn-blue']
                ) ?>
            <?php endif; ?>
            <?php if ($isAuthor && $updateEvent->status == 'created'): ?>
                <?= $this->Html->link(
                    __('Edit'),
                    ['controller' => 'UpdateEvents', 'action' => 'edit', $updateEvent->id],
                    ['class' => 'btn cdli-btn-light']
                ) ?>

                <?= $this->Form->postLink(
                    __('Submit'),
                    ['controller' => 'UpdateEvents', 'action' => 'submit', $updateEvent->id],
                    [
                        'class' => 'btn cdli-btn-light',
                        'confirm' => __('Are you sure you want to submit your drafted update?')
                    ]
                ) ?>
            <?php endif; ?>
            <?php if ($isAdmin || $isAuthor): ?>
                <?= $this->Form->postLink(
                    __('Delete'),
                    ['controller' => 'UpdateEvents', 'action' => 'delete', $updateEvent->id],
                    [
                        'class' => 'btn btn-danger float-right',
                        'confirm' => $updateEvent->status == 'created'
                        ? __('Are you sure you want to delete this drafted update?')
                        : __('Are you sure you want to retract this submitted update?')
                    ]
                    ) ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <div class="col-lg-12 boxed">
        <?php if ($updateEvent->update_type == 'artifact' && !empty($updateEvent->artifacts_updates)) {
            echo $this->element('artifactUpdateDiffStat', ['artifacts_updates' => $updateEvent->artifacts_updates]);
        } else if (!empty($updateEvent->inscriptions)) {
            echo $this->element('inscriptionDiffStat', ['inscriptions' => $updateEvent->inscriptions]);
        }?>

        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
