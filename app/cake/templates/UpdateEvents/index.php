<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-12 boxed">
        <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead>
                <tr align="left">
                    <th scope="col"><?= h('Description') ?></th>
                    <th scope="col"><?= h('Project') ?></th>
                    <th scope="col"><?= h('Type') ?></th>
                    <th scope="col"><?= h('Creator') ?></th>
                    <th scope="col"><?= h('Created') ?></th>
                    <th scope="col"><?= h('Status') ?></th>
                    <th scope="col"><?= h('Approved by') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($updateEvents as $updateEvent): ?>
                    <tr align="left">
                        <td><?= h($updateEvent->event_comments) ?></td>
                        <td>
                            <?php if ($updateEvent->has('external_resource')): ?>
                                <?= $this->Html->link(
                                    h($updateEvent->external_resource->external_resource),
                                    ['controller' => 'ExternalResources', 'action' => 'view', $updateEvent->external_resource->id]
                                ) ?>
                            <?php endif; ?>
                        </td>
                        <td><?= h($updateEvent->update_type) ?></td>
                        <td><?= $this->Html->link(
                            h($updateEvent->creator->author),
                            ['controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
                        ) ?></td>
                        <td><?= $this->Html->link(
                            h($updateEvent->created),
                            ['controller' => 'UpdateEvents', 'action' => 'view', $updateEvent->id]
                        ) ?></td>

                        <td><?= h($updateEvent->status) ?></td>
                        <td>
                            <?php if ($updateEvent->has('reviewer')): ?>
                                <?= $this->Html->link(
                                    h($updateEvent->reviewer->author),
                                    ['controller' => 'Authors', 'action' => 'view', $updateEvent->reviewer->id]
                                ) ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
