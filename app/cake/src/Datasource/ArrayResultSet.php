<?php
namespace App\Datasource;

use Cake\Collection\CollectionInterface;
use Cake\Datasource\ResultSetInterface;

class ArrayResultSet implements ResultSetInterface
{
    protected $_array = [];
    protected $_cursor = 0;

    public function __construct(array $array)
    {
        $this->_array = $array;
    }

    // implements Cake\Collection\CollectionInterface
    // https://api.cakephp.org/4.2/interface-Cake.Collection.CollectionInterface.html
    public function append($items): CollectionInterface
    {
        if (!is_array($items)) {
            $items = iterator_to_array($items);
        }

        $this->_array = array_merge($this->_array, $items);
        return $this;
    }

    public function appendItem(mixed $item, mixed $key = null): CollectionInterface
    {
        if (is_null($key)) {
            $this->_array[] = $item;
        } else {
            $this->_array[$key] = $item;
        }
        return $this;
    }

    // TODO: change `mixed $path` to `string|callable|null $path` when the linter supports it
    public function avg(mixed $path = null)
    {
        // TODO
    }

    public function buffered(): CollectionInterface
    {
        return $this;
    }

    // TODO: change `mixed $operation` to `callable|null $operation` when the linter supports it
    // TODO: change `mixed $filter` to `callable|null $filter` when the linter supports it
    public function cartesianProduct(mixed $operation = null, mixed $filter = null): CollectionInterface
    {
        // TODO
    }

    public function chunk(int $chunkSize): CollectionInterface
    {
        $this->_array = array_chunk($this->_array, $chunkSize);
        return $this;
    }

    public function chunkWithKeys(int $chunkSize, bool $preserveKeys = true): CollectionInterface
    {
        $this->_array = array_chunk($this->_array, $chunkSize, $preserveKeys);
        return $this;
    }

    // TODO: change `mixed $keyPath` to `callable|string $keyPath` when the linter supports it
    // TODO: change `mixed $valuePath` to `callable|string $valuePath` when the linter supports it
    // TODO: change `mixed $groupPath` to `callable|string|null $groupPath` when the linter supports it
    public function combine(mixed $keyPath, mixed $valuePath, mixed $groupPath = null): CollectionInterface
    {
        // TODO
    }

    public function compile(bool $preserveKeys = true): CollectionInterface
    {
        return $this;
    }

    public function contains(mixed $value): bool
    {
        return in_array($value, $this->_array, true);
    }

    // See below for
    // public function count()

    // TODO: change `mixed $path` to `callable|string $path` when the linter supports it
    public function countBy(mixed $path): CollectionInterface
    {
        // TODO
    }

    public function countKeys(): int
    {
        return count(array_unique(array_keys($this->_array)));
    }

    public function each(callable $callback)
    {
        foreach ($this->_array as $key => $value) {
            call_user_func($callback, $key, $value);
        }
        return $this;
    }

    public function every(callable $callback): bool
    {
        foreach ($this->_array as $key => $value) {
            if (!call_user_func($callback, $key, $value)) {
                return false;
            }
        }

        return true;
    }

    // TODO: change `mixed $path` to `callable|string $path` when the linter supports it
    public function extract(mixed $path): CollectionInterface
    {
        // TODO
    }

    public function filter(mixed $callback = null): CollectionInterface
    {
        $this->_array = array_filter($this->_array, $callback, ARRAY_FILTER_USE_BOTH);
        return $this;
    }

    public function first()
    {
        return $this->_array[array_key_first($this->_array)];
    }

    public function firstMatch(array $conditions)
    {
        // TODO
    }

    // TODO: change `mixed $path` to `callable|string $path` when the linter supports it
    public function groupBy(mixed $path): CollectionInterface
    {
        // TODO
    }

    // TODO: change `mixed $path` to `callable|string $path` when the linter supports it
    public function indexBy(mixed $path): CollectionInterface
    {
        // TODO
    }

    public function insert(string $path, mixed $values): CollectionInterface
    {
        // TODO
    }

    public function isEmpty(): bool
    {
        return empty($this->_array);
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function last()
    {
        return $this->_array[array_key_last($this->_array)];
    }

    public function lazy(): CollectionInterface
    {
        return $this;
    }

    // TODO: change `mixed $order` to `string|int $order` when the linter supports it
    // TODO: change `mixed $nestingKey` to `string|callable $nestingKey` when the linter supports it
    public function listNested(mixed $order = 'desc', mixed $nestingKey = 'children'): CollectionInterface
    {
        // TODO
    }

    public function map(mixed $callback): CollectionInterface
    {
        $this->_array = array_map($callback, $this->_array);
        return $this;
    }

    public function match(array $conditions): CollectionInterface
    {
        // TODO
    }

    // TODO: change `mixed $path` to `callable|string $path` when the linter supports it
    public function max(mixed $path, int $sort = SORT_NUMERIC)
    {
        // TODO
    }

    // TODO: change `mixed $path` to `string|callable|null $path` when the linter supports it
    public function median(mixed $path = null)
    {
        // TODO
    }

    // TODO: change `mixed $path` to `callable|string $path` when the linter supports it
    public function min(mixed $path, int $sort = SORT_NUMERIC)
    {
        // TODO
    }

    // TODO: change `mixed $idPath` to `callable|string $idPath` when the linter supports it
    // TODO: change `mixed $parentPath` to `callable|string $parentPath` when the linter supports it
    public function nest(mixed $idPath, mixed $parentPath, string $nestingKey = 'children'): CollectionInterface
    {
        // TODO
    }

    public function prepend($items): CollectionInterface
    {
        if (!is_array($items)) {
            $items = iterator_to_array($items);
        }

        $this->_array = array_merge($items, $this->_array);
        return $this;
    }

    public function prependItem(mixed $item, mixed $key = null): CollectionInterface
    {
        $prepend = [];

        if (is_null($key)) {
            $prepend[] = $item;
        } else {
            $prepend[$key] = $item;
        }

        $this->_array = $prepend + $this->_array;
        return $this;
    }

    public function reduce(callable $callback, mixed $initial = null)
    {
        return array_reduce($this->array, $callback, $initial);
    }

    public function reject(callable $callback): CollectionInterface
    {
        $this->_array = array_filter($this->_array, function ($value, $key) use ($callback) {
            return !call_user_func($callback, $value, $key);
        }, ARRAY_FILTER_USE_BOTH);
        return $this;
    }

    public function sample(int $length = 10): CollectionInterface
    {
        shuffle($this->_array);
        $this->_array = array_slice($this->_array, 0, $length);
        return $this;
    }

    public function shuffle(): CollectionInterface
    {
        shuffle($this->_array);
        return $this;
    }

    public function skip(int $length): CollectionInterface
    {
        $this->_array = array_slice($this->_array, $length);
        return $this;
    }

    public function some(callable $callback): bool
    {
        foreach ($this->_array as $key => $value) {
            if (call_user_func($callback, $key, $value)) {
                return true;
            }
        }

        return false;
    }

    // TODO: change `mixed $path` to `callable|string $path` when the linter supports it
    public function sortBy(mixed $path, int $order = \Cake\Collection\SORT_DESC, int $sort = SORT_NUMERIC): CollectionInterface
    {
        // TODO
    }

    // TODO: change `mixed $condition` to `callable|array $condition` when the linter supports it
    public function stopWhen(mixed $condition): CollectionInterface
    {
        // TODO
    }

    // TODO: change `mixed $path` to `string|callable|null $path` when the linter supports it
    public function sumOf(mixed $path = null)
    {
        // TODO
    }

    public function take(int $length = 1, int $offset = 0): CollectionInterface
    {
        $this->_array = array_slice($this->_array, $offset, $length);
        return $this;
    }

    public function takeLast(int $length): CollectionInterface
    {
        $this->_array = array_slice($this->_array, -$length);
        return $this;
    }

    public function through(callable $callback): CollectionInterface
    {
        return call_user_func($callbac, $this);
    }

    public function toArray(bool $preserveKeys = true): array
    {
        if ($preserveKeys) {
            return $this->_array;
        } else {
            return array_values($this->_array);
        }
    }

    public function toList(): array
    {
        return $this->toArray(false);
    }

    public function transpose(): CollectionInterface
    {
        // TODO
    }

    // TODO: change `mixed $callback` to `callable|null $callback` when the linter supports it
    public function unfold(mixed $callback = null): CollectionInterface
    {
        $array = $this->_array;
        if (!empty($callback)) {
            $array = array_map($callback, $array);
        }
        $this->_array = array_merge(...$array);
        return $this;
    }

    public function unwrap(): \Traversable
    {
        // TODO (???)
    }

    public function zip(iterable $items): CollectionInterface
    {
        // TODO
    }

    public function zipWith(iterable $items, $callback): CollectionInterface
    {
        // TODO
    }

    // implements Countable
    // https://www.php.net/manual/en/class.countable.php
    public function count(): int
    {
        return count($this->_array);
    }

    // implements Serializable
    // https://www.php.net/manual/en/class.serializable.php
    public function serialize()
    {
        return serialize($this->_array);
    }

    public function unserialize(string $data)
    {
        $this->_array = unserialize($data);
    }

    // implements Iterator
    // https://www.php.net/manual/en/class.iterator.php
    public function current()
    {
        return $this->_array[$this->key()];
    }

    public function key()
    {
        return array_keys($this->_array)[$this->_cursor];
    }

    public function next()
    {
        $this->_cursor += 1;
    }

    public function rewind()
    {
        $this->_cursor = 0;
    }

    public function valid()
    {
        $keys = array_keys($this->_array);
        return $this->_cursor < count($keys);
    }
}
