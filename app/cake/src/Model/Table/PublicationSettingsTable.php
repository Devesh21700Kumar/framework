<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PublicationSettings Model
 *
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsTo $Publications
 *
 * @method \App\Model\Entity\PublicationSetting newEmptyEntity()
 * @method \App\Model\Entity\PublicationSetting newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\PublicationSetting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PublicationSetting get($primaryKey, $options = [])
 * @method \App\Model\Entity\PublicationSetting findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\PublicationSetting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PublicationSetting[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\PublicationSetting|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PublicationSetting saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PublicationSetting[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\PublicationSetting[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\PublicationSetting[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\PublicationSetting[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class PublicationSettingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('publication_settings');

        $this->belongsTo('Publications', [
            'foreignKey' => 'publication_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('locale')
            ->maxLength('locale', 14)
            ->notEmptyString('locale');

        $validator
            ->scalar('setting_name')
            ->maxLength('setting_name', 255)
            ->requirePresence('setting_name', 'create')
            ->notEmptyString('setting_name');

        $validator
            ->scalar('setting_value')
            ->allowEmptyString('setting_value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['publication_id'], 'Publications'), ['errorField' => 'publication_id']);

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
