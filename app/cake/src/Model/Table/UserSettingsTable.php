<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserSettings Model
 *
 * @property \App\Model\Table\AssocsTable&\Cake\ORM\Association\BelongsTo $Assocs
 *
 * @method \App\Model\Entity\UserSetting newEmptyEntity()
 * @method \App\Model\Entity\UserSetting newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\UserSetting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserSetting get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserSetting findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\UserSetting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserSetting[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserSetting|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserSetting saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserSetting[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\UserSetting[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\UserSetting[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\UserSetting[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UserSettingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('user_settings');
        $this->belongsTo('Assocs', [
            'foreignKey' => 'assoc_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('locale')
            ->maxLength('locale', 14)
            ->notEmptyString('locale');

        $validator
            ->scalar('setting_name')
            ->maxLength('setting_name', 255)
            ->requirePresence('setting_name', 'create')
            ->notEmptyString('setting_name');

        $validator
            ->allowEmptyString('assoc_type');

        $validator
            ->scalar('setting_value')
            ->allowEmptyString('setting_value');

        $validator
            ->scalar('setting_type')
            ->maxLength('setting_type', 6)
            ->requirePresence('setting_type', 'create')
            ->notEmptyString('setting_type');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
