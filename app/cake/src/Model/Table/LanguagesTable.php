<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Languages Model
 *
 * @property \App\Model\Table\LanguagesTable&\Cake\ORM\Association\BelongsTo $ParentLanguages
 * @property \App\Model\Table\LanguagesTable&\Cake\ORM\Association\HasMany $ChildLanguages
 * @property \App\Model\Table\SignReadingsTable&\Cake\ORM\Association\HasMany $SignReadings
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsToMany $Artifacts
 *
 * @method \App\Model\Entity\Language newEmptyEntity()
 * @method \App\Model\Entity\Language newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Language[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Language get($primaryKey, $options = [])
 * @method \App\Model\Entity\Language findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Language patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Language[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Language|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Language saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Language[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Language[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Language[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Language[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class LanguagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('languages');
        $this->setDisplayField('language');
        $this->setPrimaryKey('id');

        $this->belongsTo('ParentLanguages', [
            'className' => 'Languages',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildLanguages', [
            'className' => 'Languages',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('SignReadings', [
            'foreignKey' => 'language_id'
        ]);
        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'language_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_languages'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('sequence')
            ->allowEmptyString('sequence');

        $validator
            ->scalar('language')
            ->maxLength('language', 50)
            ->requirePresence('language', 'create')
            ->notEmptyString('language')
            ->add('language', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('protocol_code')
            ->maxLength('protocol_code', 45)
            ->allowEmptyString('protocol_code');

        $validator
            ->scalar('inline_code')
            ->maxLength('inline_code', 20)
            ->allowEmptyString('inline_code');

        $validator
            ->scalar('notes')
            ->maxLength('notes', 50)
            ->allowEmptyString('notes');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['language']), ['errorField' => 'language']);
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['parent_id'], 'ParentLanguages'), ['errorField' => 'parent_id']);

        return $rules;
    }
}
