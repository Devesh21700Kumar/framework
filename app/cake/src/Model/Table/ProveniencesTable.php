<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Proveniences Model
 *
 * @property \App\Model\Table\RegionsTable&\Cake\ORM\Association\BelongsTo $Regions
 * @property \App\Model\Table\ArchivesTable&\Cake\ORM\Association\HasMany $Archives
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\HasMany $Artifacts
 * @property \App\Model\Table\DynastiesTable&\Cake\ORM\Association\HasMany $Dynasties
 * @property \App\Model\Table\SignReadingsTable&\Cake\ORM\Association\HasMany $SignReadings
 *
 * @method \App\Model\Entity\Provenience newEmptyEntity()
 * @method \App\Model\Entity\Provenience newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Provenience[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Provenience get($primaryKey, $options = [])
 * @method \App\Model\Entity\Provenience findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Provenience patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Provenience[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Provenience|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Provenience saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ProveniencesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('proveniences');
        $this->setDisplayField('provenience');
        $this->setPrimaryKey('id');

        $this->belongsTo('Regions', [
            'foreignKey' => 'region_id'
        ]);
        $this->hasMany('Archives', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->hasMany('Artifacts', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->hasMany('Dynasties', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->hasMany('SignReadings', [
            'foreignKey' => 'provenience_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('provenience')
            ->maxLength('provenience', 100)
            ->allowEmptyString('provenience');

        $validator
            ->scalar('geo_coordinates')
            ->maxLength('geo_coordinates', 50)
            ->allowEmptyString('geo_coordinates');

        $validator
            ->scalar('ancient_name')
            ->maxLength('ancient_name', 255)
            ->requirePresence('ancient_name', 'create')
            ->notEmptyString('ancient_name');

        $validator
            ->scalar('modern_name')
            ->maxLength('modern_name', 255)
            ->requirePresence('modern_name', 'create')
            ->notEmptyString('modern_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['region_id'], 'Regions'), ['errorField' => 'region_id']);

        return $rules;
    }
}
