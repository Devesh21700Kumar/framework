<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Inscription Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property string|null $atf
 * @property string|null $jtf
 * @property string|null $transliteration
 * @property string|null $transliteration_clean
 * @property string|null $transliteration_sign_names
 * @property string|null $transliteration_for_search
 * @property string|null $annotation
 * @property bool|null $is_atf2conll_diff_resolved
 * @property string|null $comments
 * @property string|null $structure
 * @property string|null $translation
 * @property string|null $transcription
 * @property int|null $update_event_id
 * @property string|null $inscription_comments
 * @property bool $is_latest
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\UpdateEvent $update_event
 */
class Inscription extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'atf' => true,
        'jtf' => true,
        'transliteration' => true,
        'transliteration_clean' => true,
        'transliteration_sign_names' => true,
        'transliteration_for_search' => true,
        'annotation' => true,
        'is_atf2conll_diff_resolved' => true,
        'comments' => true,
        'structure' => true,
        'translation' => true,
        'transcription' => true,
        'update_event_id' => true,
        'inscription_comments' => true,
        'is_latest' => true,
        'artifact' => true,
        'update_event' => true,
    ];

    public function getCidocCrm()
    {
        $inscription = [
            '@id' => $this->getUri(),
            '@type' => [
                'crm:E34_Inscription',
                'dcmitype:Text'
            ],
            'crm:P128i_is_carried_by' => self::getEntity($this->artifact),
            'crm:P3_has_note' => $this->atf
            // TODO
        ];

        if (!empty($this->artifact)) {
            $inscription['crm:P72_has_language'] = self::getEntities($this->artifact->languages);
            $inscription['crm:P148i_is_component_of'] = self::getEntities($this->artifact->composites);
            $inscription['crm:P67_refers_to'] = self::getEntities($this->artifact->dates);
        }

        return $inscription;
    }
}
