<?php
namespace App\Model\Entity;

use App\Model\Entity\Artifact;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * ArtifactsUpdate Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property string|null $cdli_comments
 * @property string|null $designation
 * @property string|null $artifact_type
 * @property string|null $period
 * @property string|null $provenience
 * @property string|null $written_in
 * @property string|null $archive
 * @property string|null $composite_no
 * @property string|null $seal_no
 * @property string|null $composites
 * @property string|null $seals
 * @property string|null $museum_no
 * @property string|null $accession_no
 * @property string|null $condition_description
 * @property string|null $artifact_preservation
 * @property string|null $period_comments
 * @property string|null $provenience_comments
 * @property string|null $is_provenience_uncertain
 * @property string|null $is_period_uncertain
 * @property string|null $is_artifact_type_uncertain
 * @property string|null $is_school_text
 * @property string|null $height
 * @property string|null $thickness
 * @property string|null $width
 * @property string|null $weight
 * @property string|null $elevation
 * @property string|null $excavation_no
 * @property string|null $findspot_square
 * @property string|null $findspot_comments
 * @property string|null $stratigraphic_level
 * @property string|null $surface_preservation
 * @property string|null $artifact_comments
 * @property string|null $seal_information
 * @property string|null $accounting_period
 * @property string|null $is_public
 * @property string|null $is_atf_public
 * @property string|null $are_images_public
 * @property string|null $collections
 * @property string|null $dates
 * @property string|null $alternative_years
 * @property string|null $external_resources
 * @property string|null $external_resources_key
 * @property string|null $genres
 * @property string|null $genres_comment
 * @property string|null $genres_uncertain
 * @property string|null $languages
 * @property string|null $languages_uncertain
 * @property string|null $materials
 * @property string|null $materials_aspect
 * @property string|null $materials_color
 * @property string|null $materials_uncertain
 * @property string|null $shadow_cdli_comments
 * @property string|null $shadow_collection_location
 * @property string|null $shadow_collection_comments
 * @property string|null $shadow_acquisition_history
 * @property string|null $publications_key
 * @property string|null $publications_type
 * @property string|null $publications_exact_ref
 * @property string|null $publications_comment
 * @property string|null $retired
 * @property string|null $has_fragments
 * @property string|null $is_artifact_fake
 * @property int $update_events_id
 * @property string $publication_error
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\UpdateEvent $update_event
 */
class ArtifactsUpdate extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'cdli_comments' => true,
        'designation' => true,
        'artifact_type' => true,
        'period' => true,
        'provenience' => true,
        'written_in' => true,
        'archive' => true,
        'composite_no' => true,
        'seal_no' => true,
        'composites' => true,
        'seals' => true,
        'museum_no' => true,
        'accession_no' => true,
        'condition_description' => true,
        'artifact_preservation' => true,
        'period_comments' => true,
        'provenience_comments' => true,
        'is_provenience_uncertain' => true,
        'is_period_uncertain' => true,
        'is_artifact_type_uncertain' => true,
        'is_school_text' => true,
        'height' => true,
        'thickness' => true,
        'width' => true,
        'weight' => true,
        'elevation' => true,
        'excavation_no' => true,
        'findspot_square' => true,
        'findspot_comments' => true,
        'stratigraphic_level' => true,
        'surface_preservation' => true,
        'artifact_comments' => true,
        'seal_information' => true,
        'accounting_period' => true,
        'is_public' => true,
        'is_atf_public' => true,
        'are_images_public' => true,
        'collections' => true,
        'dates' => true,
        'alternative_years' => true,
        'external_resources' => true,
        'external_resources_key' => true,
        'genres' => true,
        'genres_comment' => true,
        'genres_uncertain' => true,
        'languages' => true,
        'languages_uncertain' => true,
        'materials' => true,
        'materials_aspect' => true,
        'materials_color' => true,
        'materials_uncertain' => true,
        'shadow_cdli_comments' => true,
        'shadow_collection_location' => true,
        'shadow_collection_comments' => true,
        'shadow_acquisition_history' => true,
        'publications_key' => true,
        'publications_type' => true,
        'publications_exact_ref' => true,
        'publications_comment' => true,
        'retired' => true,
        'has_fragments' => true,
        'is_artifact_fake' => true,
        'update_events_id' => true,
        'publication_error' => true,
        'artifact' => true,
        'update_event' => true,
    ];

    private $_metaFields = [
        'id',
        'artifact_id',
        'update_events_id',
        'publication_error',
        'artifact',
        'update_event',
    ];

    private $_mappings = [
        'cdli_comments' => [],
        'designation' => [],
        'artifact_type' => ['table' => 'ArtifactTypes', 'targetKey' => 'artifact_type'],
        'period' => ['table' => 'Periods', 'targetKey' => 'period'],
        'provenience' => ['table' => 'Proveniences', 'targetKey' => 'provenience'],
        'written_in' => [
            'key' => 'origin',
            'table' => 'Proveniences',
            'targetKey' => 'provenience'
        ],
        'archive' => ['table' => 'Archives', 'targetKey' => 'archive'],
        'composite_no' => [],
        'seal_no' => [],
        'composites' => [
            'table' => 'Artifacts',
            'targetKey' => 'composite_no',
            'joinData' => []
        ],
        'seals' => [
            'table' => 'Artifacts',
            'targetKey' => 'seal_no',
            'joinData' => []
        ],
        'museum_no' => [],
        'accession_no' => [],
        'condition_description' => [],
        'artifact_preservation' => [],
        'period_comments' => [],
        'provenience_comments' => [],
        'is_provenience_uncertain' => ['type' => 'bool'],
        'is_period_uncertain' => ['type' => 'bool'],
        'is_artifact_type_uncertain' => ['type' => 'bool'],
        'is_school_text' => ['type' => 'bool'],
        'height' => ['type' => 'number'],
        'thickness' => ['type' => 'number'],
        'width' => ['type' => 'number'],
        'weight' => ['type' => 'number'],
        'elevation' => [],
        'excavation_no' => [],
        'findspot_square' => [],
        'findspot_comments' => [],
        'stratigraphic_level' => [],
        'surface_preservation' => [],
        'artifact_comments' => [],
        'seal_information' => [],
        'accounting_period' => [],
        'is_public' => ['type' => 'bool'],
        'is_atf_public' => ['type' => 'bool'],
        'are_images_public' => ['type' => 'bool'],
        'collections' => [
            'table' => 'Collections',
            'targetKey' => 'collection',
            'key' => 'collections',
            'joinData' => []
        ],
        'dates' => [],
        'alternative_years' => [],
        'external_resources' => [
            'table' => 'ExternalResources',
            'targetKey' => 'abbrev',
            'key' => 'external_resources',
            'joinData' => [
                'external_resources_key' => ['key' => 'external_resource_key'],
            ]
        ],
        'genres' => [
            'table' => 'Genres',
            'targetKey' => 'genre',
            'key' => 'genres',
            'joinData' => [
                'genres_comment' => ['key' => 'comments'],
                'genres_uncertain' => [
                    'key' => 'is_genre_uncertain',
                    'type' => 'bool'
                ]
            ]
        ],
        'languages' => [
            'table' => 'Languages',
            'targetKey' => 'language',
            'key' => 'languages',
            'joinData' => [
                'languages_uncertain' => [
                    'key' => 'is_language_uncertain',
                    'type' => 'bool'
                ]
            ]
        ],
        'materials' => [
            'table' => 'Materials',
            'targetKey' => 'material',
            'key' => 'materials',
            'joinData' => [
                'materials_aspect' => [
                    'key' => 'material_aspect',
                    'table' => 'MaterialAspects',
                    'targetKey' => 'material_aspect'
                ],
                'materials_color' => [
                    'key' => 'material_color',
                    'table' => 'MaterialColors',
                    'targetKey' => 'material_color'
                ],
                'materials_uncertain' => [
                    'key' => 'is_material_uncertain',
                    'type' => 'bool'
                ]
            ]
        ],
        'publications_key' => [
            'table' => 'Publications',
            'targetKey' => 'bibtexkey',
            'key' => 'publications',
            'joinData' => [
                'publications_type' => ['key' => 'publication_type'],
                'publications_exact_ref' => ['key' => 'exact_reference'],
                'publications_comment' => ['key' => 'publication_comments']
            ]
        ],
        'retired' => ['type' => 'bool'],
        'has_fragments' => ['type' => 'bool'],
        'is_artifact_fake' => ['type' => 'bool']
    ];

    private $_shadowJoinData = [
        'shadow_cdli_comments' => 'cdli_comments',
        'shadow_collection_location' => 'collection_location',
        'shadow_collection_comments' => 'collection_comments',
        'shadow_acquisition_history' => 'acquisition_history'
    ];

    /**
     * @param \App\Model\Entity\Artifact $artifact
     * @return bool
     */
    public function apply(\App\Model\Entity\Artifact $artifact)
    {
        if (!$this->validate()) {
            return false;
        }

        foreach ($this->_mappings as $key => $mapping) {
            $values;

            if ($this->has($key)) {
                $values = $this->get($key);
            } elseif ($this->doesChangeJoinData($mapping)) {
                TableRegistry::get('Artifacts')->loadInto($artifact, [$mapping['table']]);
                $values = $artifact->get($mapping['key']);
            } else {
                continue;
            }

            $artifact->set(
                array_key_exists('key', $mapping) ? $mapping['key'] : $key,
                $this->mapValues($values, $mapping)
            );
        }

        // Special case for materials
        if ($artifact->has('materials')) {
            foreach ($artifact->materials as $material) {
                if (!empty($material->_joinData) && $material->_joinData->has('material_color')) {
                    $material->_joinData->material_color_id = $material->_joinData->material_color->id;
                }
                if (!empty($material->_joinData) && $material->_joinData->has('material_aspect')) {
                    $material->_joinData->material_aspect_id = $material->_joinData->material_aspect->id;
                }
            }
        }

        // Special case for artifact shadows
        if ($this->doesChangeJoinData(['joinData' => $this->_shadowJoinData])) {
            $Artifacts = TableRegistry::get('Artifacts');
            $ArtifactsShadow = TableRegistry::get('ArtifactsShadow');

            $Artifacts->loadInto($artifact, ['ArtifactsShadow']);
            $shadows = $artifact->has('artifacts_shadow')
                ? $artifact->artifacts_shadow
                : [];

            // Collect values
            $joinData = [];
            $newCount;
            foreach ($this->_shadowJoinData as $key => $targetKey) {
                if ($this->has($key)) {
                    $joinData[$targetKey] = preg_split('/; */', $this->get($key));
                    $newCount = count($joinData[$targetKey]);
                }
            }

            // Match the amount of artifact shadows to the new list
            $shadowCount = count($shadows);
            if ($shadowCount < $newCount) {
                for ($i = $shadowCount; $i < $newCount; $i++) {
                    $shadow = $ArtifactsShadow->newEmptyEntity();
                    $shadow->set('artifact_id', $artifact->id);
                    $shadows[] = $shadow;
                }
            } elseif ($newCount < $shadowCount) {
                for ($i = $newCount; $i < $shadowCount; $i++) {
                    array_pop($shadows);
                }
            }

            // Apply shadow data
            foreach ($shadows as $index => $shadow) {
                foreach ($joinData as $key => $values) {
                    $shadow->set($key, $values[$index]);
                }
            }
        }

        return true;
    }

    /**
     * @param string $mapping
     * @return bool
     */
    public function doesChangeJoinData($mapping)
    {
        if (!array_key_exists('joinData', $mapping)) {
            return false;
        }

        return !empty(array_intersect(array_keys($mapping['joinData']), $this->getChanged()));
    }

    /**
     * @param $values
     * @param array $mapping
     * @return mixed
     */
    private function mapValues($values, array $mapping)
    {
        if (array_key_exists('joinData', $mapping)) {
            // Get joinData (such as publication comments, material aspects, etc.)
            $joinData = [];
            foreach ($mapping['joinData'] as $key => $subMapping) {
                if ($this->has($key)) {
                    $newKey = array_key_exists('key', $subMapping) ? $subMapping['key'] : $key;
                    $joinData[$newKey] = array_map(function ($value) use ($subMapping) {
                        return $this->mapValue($value, $subMapping);
                    }, preg_split('/; */', $this->get($key)));
                }
            }

            $entities = [];

            if (is_array($values)) {
                $entities = $values;
            } else {
                // Get individual publications/materials/genres/collections/etc.
                foreach (preg_split('/; */', $values) as $value) {
                    $entities[] = $this->mapValue($value, $mapping);
                }
            }

            foreach ($entities as $index => $entity) {
                if (!empty($joinData)) {
                    $entity->_joinData = new Entity;
                }

                foreach ($joinData as $key => $values) {
                    $entity->_joinData->set($key, $values[$index]);
                }
            }

            return $entities;
        } else {
            return $this->mapValue($values, $mapping);
        }
    }

    /**
     * @param string $value
     * @param array $mapping
     * @return mixed
     */
    private function mapValue(string $value, array $mapping)
    {
        if ($value == '') {
            return null;
        } elseif (array_key_exists('table', $mapping)) {
            // Search for matching items
            $table = TableRegistry::get($mapping['table']);
            return $table->find()
                ->where([$mapping['targetKey'] => $value])
                ->first();
        } elseif (array_key_exists('type', $mapping)) {
            if ($mapping['type'] == 'number') {
                return floatval($value);
            } elseif ($mapping['type'] == 'bool') {
                return boolval($value);
            }
        }

        return $value;
    }

    /**
     * @return boolean
     */
    public function validate()
    {
        foreach ($this->_mappings as $key => $mapping) {
            if ($this->has($key)) {
                $this->setError($key, [], true);
                $this->validateValues($key, $this->get($key), $mapping);
            }
        }

        // Special case for artifact shadows
        if ($this->has('shadow_cdli_comments')) {
            // Validate columns are of equal length
            $shadowCount;
            foreach (array_keys($this->_shadowJoinData) as $key) {
                if ($this->has($key)) {
                    $values = preg_split('/; */', $this->get($key));

                    $this->setError($key, [], true);
                    if (empty($shadowCount)) {
                        $shadowCount = count($values);
                    } elseif (count($values) !== $shadowCount) {
                        $this->setError($key, ['Too many or few values given']);
                    }
                }
            }
        }

        return !$this->hasErrors();
    }

    /**
     * @param string $values
     * @param array $mapping
     * @return mixed
     */
    private function validateValues(string $key, string $values, array $mapping)
    {
        if (array_key_exists('joinData', $mapping)) {
            // Get individual publications/materials/genres/collections/etc.
            $input = preg_split('/; */', $values);

            foreach ($input as $value) {
                $this->validateValue($key, $value, $mapping);
            }

            // Get joinData (such as publication comments, material aspects, etc.)
            $inputCount = count($input);
            foreach ($mapping['joinData'] as $key => $mapping) {
                if ($this->has($key)) {
                    $values = preg_split('/; */', $this->get($key));

                    // Validate joinData length (should be as long as $input)
                    if (count($values) !== $inputCount) {
                        $this->setError($key, ['Too many or few values given in ' . $key]);
                    } else {
                        foreach ($values as $value) {
                            $this->validateValue($key, $value, $mapping);
                        }
                    }
                }
            }
        } else {
            $this->validateValue($key, $values, $mapping);
        }
    }

    /**
     * @param string $key
     * @param string $value
     * @param array $mapping
     * @return mixed
     */
    private function validateValue(string $key, string $value, array $mapping)
    {
        if ($value == '') {
            // Do not attempt to validate empty values
        } elseif (array_key_exists('table', $mapping)) {
            // Search for matching items
            $table = TableRegistry::getTableLocator()->get($mapping['table']);
            $query = $table->find()->where([$mapping['targetKey'] => $value]);
            $resultCount = $query->count();

            // Ensure only one item matches
            if ($resultCount > 1) {
                $this->setError($key, ['Ambiguous results for "' . $value . '"']);
            } elseif ($resultCount < 1) {
                $this->setError($key, ['No results for "' . $value . '"']);
            }
        } elseif (array_key_exists('type', $mapping)) {
            if ($mapping['type'] == 'number' && !is_numeric($value)) {
                $this->setError($key, ['Not a number']);
            } elseif ($mapping['type'] == 'bool' && $value != '0' && $value != '1') {
                $this->setError($key, ['Not a boolean']);
            }
        }
    }

    /**
     * @return string[]
     */
    public function getChanged()
    {
        $metaFields = array_flip($this->_metaFields);
        return array_filter($this->getVisible(), function ($field) use ($metaFields) {
            return !array_key_exists($field, $metaFields) && $this->has($field);
        });
    }

    /**
     * @return string
     */
    public function getDesignation()
    {
        if (!empty($this->artifact_id)) {
            $artifact;
            if (empty($this->artifact)) {
                $artifact = TableRegistry::get('Artifacts')
                    ->find()
                    ->select(['id', 'designation'])
                    ->where(['id' => $this->artifact_id])
                    ->first();
            } else {
                $artifact = $this->artifact;
            }

            $cdliNumber = $artifact->getCdliNumber();
            $designation = $artifact->designation;
        }

        if (!empty($this->designation)) {
            $designation = $this->designation;
        }

        if (empty($designation)) {
            return 'no designation';
        } elseif (empty($cdliNumber)) {
            return $designation;
        } else {
            return $designation . ' (' . $cdliNumber . ')';
        }
    }
}
