<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Query Entity
 *
 * @property int $query_id
 * @property int $assoc_type
 * @property int $assoc_id
 * @property int $stage_id
 * @property float $seq
 * @property \Cake\I18n\FrozenTime|null $date_posted
 * @property \Cake\I18n\FrozenTime|null $date_modified
 * @property int $closed
 *
 * @property \App\Model\Entity\Note[] $notes
 */
class Query extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'assoc_type' => true,
        'assoc_id' => true,
        'stage_id' => true,
        'seq' => true,
        'date_posted' => true,
        'date_modified' => true,
        'closed' => true,
        'notes' => true,
    ];
}
