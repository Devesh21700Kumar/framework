<?php
namespace App\Model\Entity;

trait TableExportTrait
{
    public function serializeDisplay($variable, $display)
    {
        return empty($variable) ? null : $variable[$display];
    }

    public function serializeList($list, $key)
    {
        return $this->serializeListMap($list, function ($item) use ($key) {
            return $item[$key];
        });
    }

    public function serializeListMap($list, $map)
    {
        return empty($list) ? null : implode('; ', array_map($map, $list));
    }
}
