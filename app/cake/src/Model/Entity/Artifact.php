<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use App\Model\Entity\TableExportTrait;

/**
 * Artifact Entity
 *
 * @property int $id
 * @property string|null $ark_no
 * @property string|null $cdli_comments
 * @property string|null $composite_no
 * @property string|null $condition_description
 * @property string|null $designation
 * @property string|null $elevation
 * @property string|null $excavation_no
 * @property string|null $findspot_comments
 * @property string|null $findspot_square
 * @property string|null $museum_no
 * @property string|null $artifact_preservation
 * @property bool|null $is_public
 * @property bool|null $is_atf_public
 * @property bool|null $are_images_public
 * @property string|null $seal_no
 * @property string|null $seal_information
 * @property string|null $stratigraphic_level
 * @property string|null $surface_preservation
 * @property string|null $thickness
 * @property string|null $height
 * @property string|null $width
 * @property string|null $weight
 * @property int|null $provenience_id
 * @property int|null $period_id
 * @property bool|null $is_provenience_uncertain
 * @property bool|null $is_period_uncertain
 * @property int|null $artifact_type_id
 * @property string|null $accession_no
 * @property string|null $alternative_years
 * @property string|null $period_comments
 * @property string|null $provenience_comments
 * @property bool|null $is_school_text
 * @property int|null $written_in
 * @property bool|null $is_artifact_type_uncertain
 * @property int|null $archive_id
 * @property string|null $dates_referenced
 * @property string|null $accounting_period
 * @property string|null $artifact_comments
 * @property int|null $created_by
 * @property bool $retired
 * @property bool $has_fragments
 * @property int $is_artifact_fake
 *
 * @property \App\Model\Entity\Provenience $provenience
 * @property \App\Model\Entity\Provenience $origin
 * @property \App\Model\Entity\Period $period
 * @property \App\Model\Entity\ArtifactType $artifact_type
 * @property \App\Model\Entity\Archive $archive
 * @property \App\Model\Entity\ArtifactsShadow[] $artifacts_shadow
 * @property \App\Model\Entity\ArtifactsUpdate[] $artifacts_updates
 * @property \App\Model\Entity\Inscription $inscription
 * @property \App\Model\Entity\Posting[] $postings
 * @property \App\Model\Entity\RetiredArtifact[] $retired_artifacts
 * @property \App\Model\Entity\Collection[] $collections
 * @property \App\Model\Entity\Date[] $dates
 * @property \App\Model\Entity\ExternalResource[] $external_resources
 * @property \App\Model\Entity\Genre[] $genres
 * @property \App\Model\Entity\Language[] $languages
 * @property \App\Model\Entity\Material[] $materials
 * @property \App\Model\Entity\MaterialAspect[] $material_aspects
 * @property \App\Model\Entity\MaterialColor[] $material_colors
 * @property \App\Model\Entity\Publication[] $publications
 * @property \App\Model\Entity\Artifact[] $witnesses
 * @property \App\Model\Entity\Artifact[] $composites
 * @property \App\Model\Entity\Artifact[] $impressions
 * @property \App\Model\Entity\Artifact[] $seals
 */
class Artifact extends Entity
{
    use TableExportTrait;
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ark_no' => true,
        'cdli_comments' => true,
        'composite_no' => true,
        'condition_description' => true,
        'designation' => true,
        'elevation' => true,
        'excavation_no' => true,
        'findspot_comments' => true,
        'findspot_square' => true,
        'museum_no' => true,
        'artifact_preservation' => true,
        'is_public' => true,
        'is_atf_public' => true,
        'are_images_public' => true,
        'seal_no' => true,
        'seal_information' => true,
        'stratigraphic_level' => true,
        'surface_preservation' => true,
        'thickness' => true,
        'height' => true,
        'width' => true,
        'weight' => true,
        'provenience_id' => true,
        'period_id' => true,
        'is_provenience_uncertain' => true,
        'is_period_uncertain' => true,
        'artifact_type_id' => true,
        'accession_no' => true,
        'alternative_years' => true,
        'period_comments' => true,
        'provenience_comments' => true,
        'is_school_text' => true,
        'written_in' => true,
        'is_artifact_type_uncertain' => true,
        'archive_id' => true,
        'dates_referenced' => true,
        'accounting_period' => true,
        'artifact_comments' => true,
        'created_by' => true,
        'retired' => true,
        'has_fragments' => true,
        'is_artifact_fake' => true,
        'provenience' => true,
        'origin' => true,
        'period' => true,
        'artifact_type' => true,
        'archive' => true,
        'artifacts_shadow' => true,
        'artifacts_updates' => true,
        'inscription' => true,
        'postings' => true,
        'retired_artifacts' => true,
        'collections' => true,
        'dates' => true,
        'external_resources' => true,
        'genres' => true,
        'languages' => true,
        'materials' => true,
        'material_aspects' => true,
        'material_colors' => true,
        'publications' => true,
        'witnesses' => true,
        'composites' => true,
        'impressions' => true,
        'seals' => true,
    ];

    public function getFlatValue(string $key)
    {
        switch ($key) {
            case 'cdli_comments': return null;
            case 'artifact_id': return $this->id;
            case 'dates': return $this->dates_referenced;

            case 'artifact_type': return $this->serializeDisplay($this->artifact_type, 'artifact_type');
            case 'period': return $this->serializeDisplay($this->period, 'period');
            case 'provenience': return $this->serializeDisplay($this->provenience, 'provenience');
            case 'written_in': return $this->serializeDisplay($this->origin, 'provenience');
            case 'archive': return $this->serializeDisplay($this->archive, 'archive');
            case 'composites': return $this->serializeList($this->composites, 'composite_no');
            case 'seals': return $this->serializeList($this->seals, 'seal_no');
            case 'collections': return $this->serializeList($this->collections, 'collection');

            // External resources
            case 'external_resources': return $this->serializeList($this->external_resources, 'external_resource');
            case 'external_resources_key': return $this->serializeListMap(
                $this->external_resources,
                function ($external_resource) {
                    return $external_resource->_joinData->external_resource_key;
                }
            );

            // Genres
            case 'genres': return $this->serializeList($this->genres, 'genre');
            case 'genres_comment': return $this->serializeListMap(
                $this->genres,
                function ($genre) {
                    return $genre->_joinData->comments;
                }
            );
            case 'genres_uncertain': return $this->serializeListMap(
                $this->genres,
                function ($genre) {
                    return $genre->_joinData->is_genre_uncertain;
                }
            );

            // Languages
            case 'languages': return $this->serializeList($this->languages, 'language');
            case 'languages_uncertain': return $this->serializeListMap(
                $this->languages,
                function ($language) {
                    return $language->_joinData->is_language_uncertain;
                }
            );

            // Materials
            case 'materials': return $this->serializeList($this->materials, 'material');
            case 'materials_aspect': return $this->serializeListMap(
                $this->materials,
                function ($material) {
                    return $material->_joinData->material_aspect;
                }
            );
            case 'materials_color': return $this->serializeListMap(
                $this->materials,
                function ($material) {
                    return $material->_joinData->material_color;
                }
            );
            case 'materials_uncertain': return $this->serializeListMap(
                $this->materials,
                function ($material) {
                    return $material->_joinData->is_material_uncertain;
                }
            );

            // Shadows
            case 'shadow_cdli_comments': return $this->serializeList($this->artifacts_shadow, 'cdli_comments');
            case 'shadow_collection_location': return $this->serializeList($this->artifacts_shadow, 'collection_location');
            case 'shadow_collection_comments': return $this->serializeList($this->artifacts_shadow, 'collection_comments');
            case 'shadow_acquisition_history': return $this->serializeList($this->artifacts_shadow, 'acquisition_history');

            // Publications
            case 'publications_key': return $this->serializeList($this->publications, 'bibtexkey');
            case 'publications_type': return $this->serializeListMap(
                $this->publications,
                function ($publication) {
                    return $publication->_joinData->publication_type;
                }
            );
            case 'publications_exact_ref': return $this->serializeListMap(
                $this->publications,
                function ($publication) {
                    return $publication->_joinData->exact_reference;
                }
            );
            case 'publications_comment': return $this->serializeListMap(
                $this->publications,
                function ($publication) {
                    return $publication->_joinData->publication_comments;
                }
            );

            // Other flat fields have the same field name in the entity
            default: return $this->has($key) ? $this->get($key) : null;
        }
    }

    public function getFlatValues(array $keys)
    {
        return array_combine($keys, array_map(function ($key) {
            return $this->getFlatValue($key);
        }, $keys));
    }

    public function getTableRow()
    {
        return $this->getFlatValues([
            'artifact_id',
            'cdli_comments',
            'designation',
            'artifact_type',
            'period',
            'provenience',
            'written_in',
            'archive',
            'composite_no',
            'seal_no',
            'composites',
            'seals',
            'museum_no',
            'accession_no',
            'condition_description',
            'artifact_preservation',
            'period_comments',
            'provenience_comments',
            'is_provenience_uncertain',
            'is_period_uncertain',
            'is_artifact_type_uncertain',
            'is_school_text',
            'height',
            'thickness',
            'width',
            'weight',
            'elevation',
            'excavation_no',
            'findspot_square',
            'findspot_comments',
            'stratigraphic_level',
            'surface_preservation',
            'artifact_comments',
            'seal_information',
            'is_public',
            'is_atf_public',
            'are_images_public',
            'collections',
            'dates',
            'alternative_years',
            'genres',
            'languages',
            'materials',
            'shadow_cdli_comments',
            'shadow_collection_location',
            'shadow_collection_comments',
            'shadow_acquisition_history',
            'publications_key',
            'publications_type',
            'publications_exact_ref',
            'publications_comment'
        ]);
    }

    public function getFlatData()
    {
        return $this->getFlatValues(array_keys(self::$flatFields));
    }

    public function getCdliNumber()
    {
        return 'P' . str_pad($this->id, 6, '0', STR_PAD_LEFT);
    }

    private static function getSeparatedIdentifiers($ids, $kind)
    {
        return array_map(function ($id) use ($kind) {
            return self::getIdentifier($id, $kind);
        }, explode(" + ", $ids));
    }

    public function getCidocCrm()
    {
        $cdli = $this->getCdliNumber();
        return [
            '@id' => $cdli,
            '@type' => empty($this->witnesses)
                ? 'crm:E84_Information_Carrier'
                : 'crm:E33_Linguistic_Object',
            'crm:P48_has_preferred_identifier' => array_merge(
                [
                    self::getIdentifier($cdli, 'cdli'),
                    self::getIdentifier($this->designation, 'designation'),
                    self::getIdentifier($this->accession_no, 'accession'),
                    self::getIdentifier($this->ark_no, 'ark'),
                    self::getIdentifier($this->composite_no, 'composite'),
                    self::getIdentifier($this->seal_no, 'seal')
                ],
                self::getSeparatedIdentifiers($this->museum_no, 'museum'),
                self::getSeparatedIdentifiers($this->excavation_no, 'excavation')
            ),
            'crm:P1_is_identified_by' => empty($this->external_resources)
                ? null
                : array_map(function ($resource) {
                    return self::getEntity($resource->_joinData);
                }, $this->external_resources),
            'crm:P43_has_dimension' => [
                self::getDimension($this->width, 'width', 'mm'),
                self::getDimension($this->height, 'height', 'mm'),
                self::getDimension($this->thickness, 'thickness', 'mm'),
                self::getDimension($this->weight, 'weight', 'g')
            ],
            'crm:P128_carries' => self::getEntity($this->inscription),
            'crm:P108i_was_produced_by' => [
                '@type' => 'crm:E12_Production',
                'crm:P10_falls_within' => self::getEntity($this->period),
                'crm:P7_took_place_at' => self::getEntity($this->origin)
            ],
            'crm:P46i_forms_part_of' => self::getEntities($this->collections),
            'crm:P67i_is_referred_to_by' => self::getEntities($this->publications),
            'crm:P44_has_condition' => [
                '@type' => 'crm:E3_Condition_State',
                'crm:P5_consists_of' => [
                    '@type' => 'crm:E3_Condition_State',
                    'rdfs:value' => $this->surface_preservation
                ],
                'rdfs:value' => $this->artifact_preservation,
                'crm:P3_has_note' => $this->condition_description
            ],
            'crm:P45_consists_of' => self::getEntities($this->materials),
            'crm:P56_bears_feature' => array_merge(
                empty($this->material_aspects) ? [] : self::getEntities($this->material_aspects),
                empty($this->material_colors) ? [] : self::getEntities($this->material_colors)
            ),
            'crm:P53_has_former_or_current_location' => self::getEntity($this->archive),
            'crm:P2_has_type' => array_merge(
                self::getEntities($this->genres),
                [
                    self::getEntity($this->artifact_type)
                ]
            ),
            'crma:AP21i_is_contained_in' => [
                '@type' => 'crma:A2_Stratigraphic_Volume_Unit',
                'crm:P1_is_identified_by' => [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->stratigraphic_level
                ],
                'crma:AP5i_was_partially_or_totally_removed_by' => [
                    '@type' => 'crma:A1_Excavation_Process_Unit',
                    'crm:P7_took_place_at' => [
                        '@type' => 'crm:E53_Place',
                        'crm:P48_has_preferred_identifier' => self::getIdentifier($this->findspot_square, 'findspot'),
                        'crm:P3_has_note' => $this->findspot_comments,
                        'crm:P89_falls_within' => self::getEntity($this->provenience)
                    ]
                ]
            ],
            'crm:P148_has_component' => empty($this->witnesses) ? null : self::getEntities(
                array_map(function ($artifact) {
                    return $artifact->inscription;
                }, $this->witnesses)
            ),
            'crm:P31i_was_modified_by' => empty($this->seals) ? null : array_map(function ($artifact) {
                return [
                    '@type' => 'crm:E11_Modification',
                    'crm:P16_used_specific_object' => $artifact
                ];
            }, self::getEntities($this->seals)),
            'crm:P16i_was_used_for' => empty($this->impressions) ? null : array_map(function ($artifact) {
                return [
                    '@type' => 'crm:E11_Modification',
                    'crm:P31_has_modified' => $artifact
                ];
            }, self::getEntities($this->impressions))
            // TODO retired
        ];
    }

    public static $flatFields = [
        'artifact_id' => null,
        'cdli_comments' => null,
        'designation' => null,
        'artifact_type' => ['ArtifactTypes'],
        'period' => ['Periods'],
        'provenience' => ['Proveniences'],
        'written_in' => ['Origins'],
        'archive' => ['Archives'],
        'composite_no' => null,
        'seal_no' => null,
        'composites' => ['Composites'],
        'seals' => ['Seals'],
        'museum_no' => null,
        'accession_no' => null,
        'condition_description' => null,
        'artifact_preservation' => null,
        'period_comments' => null,
        'provenience_comments' => null,
        'is_provenience_uncertain' => null,
        'is_period_uncertain' => null,
        'is_artifact_type_uncertain' => null,
        'is_school_text' => null,
        'height' => null,
        'thickness' => null,
        'width' => null,
        'weight' => null,
        'elevation' => null,
        'excavation_no' => null,
        'findspot_square' => null,
        'findspot_comments' => null,
        'stratigraphic_level' => null,
        'surface_preservation' => null,
        'artifact_comments' => null,
        'seal_information' => null,
        'is_public' => null,
        'is_atf_public' => null,
        'are_images_public' => null,
        'collections' => ['Collections'],
        'alternative_years' => null,
        'genres' => ['Genres'],
        'languages' => ['Languages'],
        'materials' => ['Materials'],
        'shadow_cdli_comments' => ['ArtifactsShadow'],
        'shadow_collection_location' => ['ArtifactsShadow'],
        'shadow_collection_comments' => ['ArtifactsShadow'],
        'shadow_acquisition_history' => ['ArtifactsShadow'],
        'publications_key' => ['Publications'],
        'publications_type' => ['Publications'],
        'publications_exact_ref' => ['Publications'],
        'publications_comment' => ['Publications'],
        'external_resources' => ['ExternalResources'],
        'external_resources_key' => ['ExternalResources'],
        'genres_comment' => ['Genres'],
        'genres_uncertain' => ['Genres'],
        'languages_uncertain' => ['Languages'],
        'materials_aspect' => ['Materials'],
        'materials_color' => ['Materials'],
        'materials_uncertain' => ['Materials'],
        'retired' => null,
        'has_fragments' => null,
        'is_artifact_fake' => null
    ];

    public static $privateFlatFields = [
        'is_public' => null,
        'is_atf_public' => null,
        'are_images_public' => null,
        'shadow_cdli_comments' => null,
        'shadow_collection_location' => null,
        'shadow_collection_comments' => null,
        'shadow_acquisition_history' => null
    ];
}
