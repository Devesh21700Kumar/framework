<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\I18n\Time;

/**
 * UpdateEvents Controller
 *
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UpdateEventsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('UpdateEvents');
        $this->loadModel('Users');
        $this->loadComponent('GranularAccess');

        $this->Auth->allow(['index', 'view', 'add', 'cancel', 'edit', 'delete', 'submit']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'ExternalResources',
                'Reviewers',
                'Creators'
            ],
            'order' => [
                'UpdateEvents.created' => 'DESC'
            ]
        ];

        if (!$this->GranularAccess->canReviewEdits()) {
            $this->paginate['conditions'] = [
                'UpdateEvents.status' => 'approved'
            ];
        }

        $updateEvents = $this->paginate($this->UpdateEvents);

        $this->set(compact('updateEvents'));
    }

    /**
     * View method
     *
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $event = $this->UpdateEvents->get($id, [
            'contain' => [
                'Reviewers.Users',
                'Creators.Users',
                'ExternalResources'
            ]
        ]);

        $isAdmin = $this->GranularAccess->isAdmin();
        $isReviewer = $this->GranularAccess->canReviewEdits();
        $isAuthor = !empty($event->creator->user) && $event->creator->user->id == $this->Auth->user('id');

        if (!$this->GranularAccess->canAccessEdits($event)) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'index']);
        }

        // Add artifactsUpdates and inscriptions depending on access
        $this->GranularAccess->amendUpdateEvent($event);

        $this->set('updateEvent', $event);
        $this->set('isAdmin', $isAdmin);
        $this->set('isReviewer', $isReviewer);
        $this->set('isAuthor', $isAuthor);
        $this->set('_serialize', 'updateEvent');
    }

    /**
     * @param \App\Model\Entity\UpdateEvent[] $updates
     * @param string[] $errors
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'index']);
        }

        $event = $this->UpdateEvents->newEmptyEntity();
        $creator = $this->Users->get($this->Auth->user('id'), ['contain' => ['Authors']]);
        $event->creator = $creator->author;

        // Get updates from sessions
        $session = $this->getRequest()->getSession();
        $updates = $session->read('ArtifactsUpdates.uploads');
        $inscriptions = $session->read('Inscriptions.uploads');
        if (is_array($updates)) {
            $event->artifacts_updates = $updates;
            $event->update_type = 'artifact';
        } elseif (is_array($inscriptions)) {
            $event->inscriptions = $inscriptions;
            $event->update_type = 'atf';
        }

        if ($this->request->is('post')) {
            $action = $this->request->getData('action');

            if ($action != 'created' && $action != 'submitted' && $action != 'approved') {
                throw new BadRequestException;
            }

            // Only users with reviewing privileges can approve immediately
            if ($action == 'approved' && !$this->GranularAccess->canReviewEdits()) {
                throw new ForbiddenException;
            }

            // Add metadata to event
            $event = $this->UpdateEvents->patchEntity($event, $this->request->getData());
            $event->created = Time::now();
            $event->status = $action;

            if ($action == 'approved') {
                $event->approved_by = $creator->author->id;
            }

            // Remove placeholder artifacts
            if (is_array($event->artifacts_updates)) {
                foreach ($event->artifacts_updates as $update) {
                    if (!empty($update->artifact) && $update->artifact->isNew()) {
                        unset($update->artifact);
                    }
                }
            }

            if ($this->UpdateEvents->save($event)) {
                $session->delete('ArtifactsUpdates.uploads');

                if ($action == 'approved') {
                    if ($event->apply()) {
                        $this->Flash->success(__('The update has been approved.'));
                    } else {
                        $errors = $event->getError($event->update_type == 'artifact' ? 'artifacts_updates' : 'inscriptions');
                        foreach ($errors as $error) {
                            $this->Flash->error($error);
                        }
                    }
                } else {
                    $this->Flash->success(__('The update has been ' . $event->status . '.'));
                }

                return $this->redirect(['action' => 'view', $event->id]);
            }

            $this->Flash->error(__('The update could not be ' . $event->status . '. Please, try again.'));
        }

        $external_resources = $this->UpdateEvents->ExternalResources->find('list', [
            'valueField' => ['abbrev'],
            'order' => ['abbrev' => 'ASC'],
            'limit' => 200
        ]);
        $this->set('event', $event);
        $this->set('external_resources', $external_resources);
        $this->set('canReviewEdits', $this->GranularAccess->canReviewEdits());
    }

    public function cancel()
    {
        $session = $this->getRequest()->getSession();
        $session->delete('ArtifactsUpdates.uploads');
        $this->redirect(['action' => 'index']);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        $event = $this->UpdateEvents->get($id, [
            'contain' => [
                'Reviewers',
                'Creators',
                'ExternalResources'
            ]
        ]);
        $user = $this->Users->get($this->Auth->user('id'));

        if ($event->isCreatedBy($user) && !$this->GranularAccess->isAdmin()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->patchEntity($event, $this->request->getData());

            if ($this->UpdateEvents->save($event)) {
                $this->Flash->success(__('The update has been updated.'));
                return $this->redirect(['action' => 'view', $event->id]);
            }

            $this->Flash->error(__('The update could not be updated.'));
        }

        $external_resources = $this->UpdateEvents->ExternalResources->find('list', [
            'valueField' => ['abbrev'],
            'order' => ['abbrev' => 'ASC'],
            'limit' => 200
        ]);
        $this->set('event', $event);
        $this->set('external_resources', $external_resources);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function submit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->get($id, [
                'contain' => [
                    'Reviewers',
                    'Creators',
                    'ExternalResources'
                ]
            ]);
            $user = $this->Users->get($this->Auth->user('id'));

            if ($event->isCreatedBy($user) || $this->GranularAccess->isAdmin()) {
                $event->status = 'submitted';

                if ($this->UpdateEvents->save($event)) {
                    $this->Flash->success(__('The update has been submitted.'));
                    return $this->redirect(['action' => 'view', $event->id]);
                }
            }

            $this->Flash->error(__('The update could not be submitted.'));
        }

        return $this->redirect(['action' => 'view', $id]);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function approve($id)
    {
        if (!$this->GranularAccess->canReviewEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->get($id, [
                'contain' => [
                    'Reviewers',
                    'Creators',
                    'ExternalResources'
                ]
            ]);

            $user = $this->Users->get($this->Auth->user('id'), ['contain' => ['Authors']]);
            $event->status = 'approved';
            $event->approved_by = $user->author->id;

            if ($event->apply()) {
                if ($this->UpdateEvents->save($event, ['associated' => false])) {
                    $this->Flash->success(__('The update has been approved.'));
                } else {
                    $this->Flash->error(__('The update could not be approved.'));
                }
            } else {
                $errors = $event->getError($event->update_type == 'artifact' ? 'artifacts_updates' : 'inscriptions');
                foreach ($errors as $error) {
                    $this->Flash->error($error);
                }
            }
        }

        return $this->redirect(['action' => 'view', $id]);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function delete($id = null)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->Auth->getConfig('authError'));
            return $this->redirect(['action' => 'view', $event->id]);
        }

        $this->getRequest()->allowMethod(['post', 'delete']);

        $event = $this->UpdateEvents->get($id, ['contain' => ['Creators']]);
        $user = $this->Users->get($this->Auth->user('id'));

        if ($event->status == 'approved') {
            $this->Flash->error('Cannot delete approved update.');
            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($event->isCreatedBy($user) || $this->GranularAccess->isAdmin()) {
            if ($this->UpdateEvents->delete($event)) {
                $this->Flash->success(__('The update has been deleted.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The update could not be deleted. Please, try again.'));
                return $this->redirect(['action' => 'view', $event->id]);
            }
        }

        $this->Flash->error($this->Auth->getConfig('authError'));
        return $this->redirect(['action' => 'view', $event->id]);
    }
}
