<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * ArtifactTypes Controller
 *
 * @property \App\Model\Table\ArtifactTypesTable $ArtifactTypes
 *
 * @method \App\Model\Entity\ArtifactType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactTypesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentArtifactTypes']
        ];
        $artifactTypes = $this->paginate($this->ArtifactTypes);

        $this->set(compact('artifactTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifact Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactType = $this->ArtifactTypes->get($id, [
            'contain' => ['ParentArtifactTypes', 'ChildArtifactTypes']
        ]);

        $artifacts = TableRegistry::get('Artifacts');
        $count = $artifacts->find('list', ['conditions' => ['artifact_type_id' => $id]])->count();

        $this->set(compact('artifactType', 'count'));
    }
}
