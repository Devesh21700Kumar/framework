# Bulk Upload Component Usage

This component can be used to handle bulk upload for any table from the database. While the upload for default fields is straightforward, the associated fields need to be configured to be used.

The workflow for the feature is as follows:

1. Upload file and check if the file is in the required format and has proper headers set.
2. If validation errors exist, then display the errors and asks for confirmation to proceed. Else proceed directly to saving.
3. Save all the entries and display the error report.


Currently, bulk upload has been implemented for the following tables: 
1. Publications
2. ArtifactsPublications
3. AuthorsPublications
4. EditorsPublications


### Configuration

For using the component for any table, the configurations for the specific table need to be added to `BulkUploadComponent.php` configuration variables at the start of the file.

For adding the configuration, add a key value pair with the key being the table name in PascalCase (e.g. ArtifactsPublications) to the following variables as per requirement:

1. `$all_header_mappings`: For mapping fieldname from the one specified in the upload file to the fieldname required by database.

E.g. For the ArtifactsPublications bulk upload, bibtexkey is used for the CSV file corresponding to the publication_id field in the ArtifactsPublications table.
```
public $all_header_mappings = [
    'ArtifactsPublications' => [
        'bibtexkey' => 'publication_id'
    ],
];
```

2. `$all_exclude_fields`: By default, all the fields in the table are marked as required. If any field needs to be excluded, it can be specified here.

E.g. For the Publications table.
```
public $all_exclude_fields = [
    'Publications' => ['accepted_by', 'accepted', 'update_events_id', 'editor']
];
```

3. `$all_associated_tables`: The many-to-many association tables have to be specified here.
   
E.g. For the Publications table.
```
public $all_associated_tables = [
    'Publications' => ['Authors', 'Editors']
];
```
   
4. `$all_associated_data_fields`: The field names for the associated table have to be defined here. (This is required if the associated tables are mentioned).

E.g. For the Publications table configuration mentioned above.
```
public $all_associated_data_fields = [
    'Publications' => ['authors', 'editors']
];
```

Notes:

1. The above configurations just handle the header name conversion to the save format and saving of only the mentioned associated data tables. The data format conversion needs to be handled by the users using the table model file.
2. All the fields based on the configuration need to be present in the CSV file (Ordering does not matter).

### Implementation instructions

The `TABLE_NAME` for the required data table (in PascalCase) needs to be provided to the component for using it.

1. Load the BulkUpload component in the controller of the table:

```
$this->loadComponent('BulkUpload', ['table' => TABLE_NAME]);
```

2. In the controller for the bulk upload, add the following two lines:

```
$this->BulkUpload->upload();
```

3. Also, for the export errors to work, add the following export function to the same controller file.

```
public function export()
{
    $this->BulkUpload->export();
}
```

4. Add the following lines to  the view page through which you want to implement bulk upload.

```
<?= $this->cell('BulkUpload::confirmation', [
                        TABLE_NAME,
                        isset($error_list_validation) ? $error_list_validation:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

<?= $this->cell('BulkUpload', [
                        TABLE_NAME, 
                        isset($error_list) ? $error_list:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>
```
