<?php
namespace App\Controller\Component;

use Cake\Event\EventInterface;
use Cake\Utility\Inflector;

trait FileDownloadTrait
{
    /**
     * @param \Cake\Event\EventInterface $event
     */
    public function shutdown(EventInterface $event)
    {
        $type = $this->RequestHandler->prefers();

        if (isset($this->types[$type])) {
            $controller = $event->getSubject();

            $vars = $controller->viewBuilder()->getVars();
            if (array_key_exists('_serialize', $vars)) {
                $serialized = $vars['_serialize'];
                $serialized = is_array($serialized) ? $serialized : [$serialized];
                $serialized = array_map(function ($key) use ($vars) {
                    $var = $vars[$key];
                    if ($var instanceof \Cake\ORM\ResultSet) {
                        $var = (array) $var;
                    } elseif (!is_array($var)) {
                        $var = [$var];
                    }
                    return $var;
                }, $serialized);
                $serialized = array_merge(...$serialized);

                if ($serialized[0] instanceof \Cake\ORM\Entity) {
                    $file = Inflector::dasherize($serialized[0]->getSource());

                    if (count($serialized) == 1 && $serialized[0]->has('id')) {
                        $file .= '_' . $serialized[0]->id;
                    }

                    $file .= '.' . $this->RequestHandler->prefers();

                    $controller->setResponse($controller->getResponse()->withDownload($file));
                }
            }
        }
    }
}
