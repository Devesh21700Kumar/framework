<?php
namespace App\Controller\Component;

use Cake\Controller\Component\RequestHandlerComponent as _RequestHandlerComponent;
use Cake\Event\EventInterface;
use Cake\Http\Response;
use Cake\Routing\Router;

class RequestHandlerComponent extends _RequestHandlerComponent
{
    /**
     * @param \Cake\Event\EventInterface $event Event.
     * @return \Cake\Http\Response|null|void
     */
    public function startup(EventInterface $event): void
    {
        parent::startup($event);

        $format = $this->getController()->getRequest()->getParam('format');
        if ($format) {
            $this->ext = $format;
        }
    }

    /**
     * @param string $type
     * @return array
     */
    public function accepts($type = null): array
    {
        if (!empty($this->ext)) {
            return [$this->ext];
        } else {
            return parent::accepts($type);
        }
    }

    /**
     * @param \Cake\Event\EventInterface $event
     * @param array $parts
     * @param \Cake\Http\Response $response
     * @return \Cake\Http\Response
     */
    public function beforeRedirect(EventInterface $event, $parts, Response $response): Response
    {
        $request = $this->getController()->getRequest();
        $format = $request->getParam('format');
        if ($format) {
            $parts['format'] = $format;
            return $response->withLocation(Router::url($parts));
        }
    }
}
