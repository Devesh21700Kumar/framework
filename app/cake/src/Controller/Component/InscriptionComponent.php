<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Event\Event;

class InscriptionComponent extends Component
{
    use FileDownloadTrait;

    public $components = ['RequestHandler'];

    // No official MIME types, but we need some anyway
    public $types = [
        'atf' => 'text/x-c-atf',
        'cdli-conll' => 'text/x-cdli-conll',
        'conll-u' => 'text/x-conll-u'
    ];

    public function initialize(array $config): void
    {
        $controller = $this->_registry->getController();

        foreach ($this->types as $ext => $header) {
            $controller->getResponse()->setTypeMap($ext, $header);
        }

        $this->RequestHandler->setConfig([
            'viewClassMap.atf' => 'Atf',
            'viewClassMap.cdli-conll' => 'CdliConll',
            'viewClassMap.conll-u' => 'ConllU'
        ]);
    }

    public function requestsInscription($type = null)
    {
        if (empty($type)) {
            $type = $this->RequestHandler->prefers();
        }

        return isset($this->types[$type]);
    }

    public function getPreferredType($inscription = null)
    {
        if (empty($inscription)) {
            return $this->RequestHandler->prefers();
        }

        foreach ($this->RequestHandler->accepts() as $type) {
            if ($type === 'atf' && empty($inscription->atf)) {
                continue;
            }

            if ($type === 'cdli-conll' && empty($inscription->annotation)) {
                continue;
            }

            if ($type === 'conll-u' && empty($inscription->annotation)) {
                continue;
            }

            return $type;
        }

        return null;
    }
}
