<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use PhpParser\Node\Stmt\ElseIf_;

/**
 * Images Controller
 *
 * @property \App\Model\Table\ImagesTable $Images
 * @method \App\Model\Entity\Image[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ImagesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }
        $this->paginate = [
            'contain' => ['Artifacts'],
            'order' => [
                'Images.artifact_id' => 'ASC',
                'Images.image_type' => 'ASC'
            ]
        ];
        $images = $this->paginate($this->Images);

        $this->set(compact('images'));
    }

    /**
     * View method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => [
                'Artifacts',
                'Artifacts.Proveniences',
                'Artifacts.Periods',
                'Artifacts.ArtifactTypes',
                'Artifacts.Archives',
                'Artifacts.Collections',
                'Artifacts.Genres',
                'Artifacts.Languages',
                'Artifacts.Materials',
            ],
        ]);

        $query=$this->Images->find('all', [
            'conditions'=> [
                'Images.artifact_id' => $image->artifact_id
            ]
        ]);
        $image_artifact=$query->all();
        $rows=$image_artifact->toList();

        $this->set(compact('image', $image, 'rows', $rows));
    }
}
