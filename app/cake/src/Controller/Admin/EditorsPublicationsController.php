<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class EditorsPublicationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'order' => [
                'editor_id' => 'ASC',
                'publication_id' => 'ASC'
            ],
            'contain' => ['Publications', 'Authors']
        ];
        $authorsPublications = $this->paginate($this->EditorsPublications);

        $this->set(compact('authorsPublications'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($flag = '', $id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        if ($flag == 'bulk') {
            $this->loadComponent('BulkUpload', ['table' => 'EditorsPublications']);
            $this->BulkUpload->upload();
            
            $this->set(compact('flag'));
        } else {
            $authors_list = $this->EditorsPublications->Authors->find('all');
            $authors_mapping = [];
            foreach ($authors_list as $row) {
                $authors_mapping[$row['author']] = $row['id'];
            }
            $authors_names = array_keys($authors_mapping);
            $authorsPublication = $this->EditorsPublications->newEntity();
            if ($this->getRequest()->is('post')) {
                $data = $this->getRequest()->getData();
                if (in_array($data['editor_id'], $authors_names)) {
                    $data['editor_id'] = $authors_mapping[$data['editor_id']];
                } else {
                    $data['editor_id'] = 0;
                }
                $authorsPublication = $this->EditorsPublications->patchEntity($authorsPublication, $data);
                if ($this->EditorsPublications->save($authorsPublication)) {
                    $this->Flash->success(__('New link has been saved.'));
                    return $this->redirect(['action' => 'add']);
                } else {
                    $this->Flash->error(__('The link could not be saved. Please, try again.'));
                }
            }
            $this->set(compact('flag', 'authorsPublication', 'authors_names'));
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Authors Publication id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $flag = '', $parent_id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $authorsPublication = $this->EditorsPublications->get($id, [
                        'contain' => ['Publications', 'Authors']
                    ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $authorsPublication = $this->EditorsPublications->patchEntity($authorsPublication, $this->getRequest()->getData());
            if ($this->EditorsPublications->save($authorsPublication)) {
                $this->Flash->success(__('Changes has been saved.'));

                if ($flag == '') {
                    return $this->redirect(['action' => 'index']);
                } else {
                    return $this->redirect(['controller' => 'AuthorsPublications', 'action' => 'add', $flag, $parent_id]);
                }
            }
            $this->Flash->error(__('Changes could not be saved. Please, try again.'));
        }
        $authorsPublications = $this->EditorsPublications->find('all', ['contain' => ['Authors', 'Publications']])->where(['publication_id' => $authorsPublication->publication_id])->all();
        $this->set(compact('authorsPublication', 'flag', 'parent_id', 'authorsPublications'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Authors Publication id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $flag = '', $parent_id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $authorsPublication = $this->EditorsPublications->get($id);
        if ($this->EditorsPublications->delete($authorsPublication)) {
            $this->Flash->success(__('The link has been deleted.'));
        } else {
            $this->Flash->error(__('The link could not be deleted. Please, try again.'));
        }

        if ($flag == '') {
            return $this->redirect(['action' => 'index']);
        } else {
            return $this->redirect(['controller' => 'AuthorsPublications', 'action' => 'add', $flag, $parent_id]);
        }
    }

    /**
     * Export method for downloading the entries containing errors.
     */
    public function export()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->loadComponent('BulkUpload', ['table' => 'EditorsPublications']);
        $this->BulkUpload->export();
    }
}
