<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Home Controller
 *
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * dashboard method
     *
     * @return \Cake\Http\Response|void
     */
    public function dashboard()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1, 2])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->loadModel('Artifacts');
        $this->paginate = [
            'contain' => ['Proveniences', 'Periods', 'ArtifactTypes', 'Archives']
        ];
        $artifacts = $this->paginate($this->Artifacts);

        $this->set(compact('artifacts'));
        $this->set('_serialize', ['artifacts']);
    }
}
