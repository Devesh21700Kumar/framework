<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\ForbiddenException;

/**
 * Articles Controller
 *

 *
 * @method \App\Model\Entity\Journal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Articles');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Bibliography');
        $this->loadComponent('GeneralFunctions');
        $this->loadModel('UserSettings');
        $this->loadModel('SubmissionComments');
        $this->loadModel('ReviewAssignments');
        $this->loadModel('ReviewRoundFiles');

        $this->Auth->allow(['index', 'view', 'image']);
    }

    public function index($type)
    {
        if ($type == 'cdlp') {
            $this->paginate = [
                'order' =>[
                    'Articles.id' => 'ASC'
                ]
            ];
        } else {
            $this->paginate = [
                'order' =>[
                    'Articles.created' => 'DESC'
                ]
            ];
        };
        $this->paginate = [
            'contain' => ['Authors'],
        ];
        $articles = $this->paginate($this->Articles->find('type', ['type' => $type]));
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1,2]);

        $this->set(compact('articles', 'type', 'access_granted'));
        $this->set('_serialize', 'articles');
    }

    public function view($id)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Authors',
                          'ReviewRounds',
                          'ReviewRounds.ReviewAssignments',
                          'Queries',
                          'Queries.Notes']
        ]);
        if (!empty($article->submission_id)) {
            $ojs_files = $this->ReviewRoundFiles->find('all', array('contain' => 'SubmissionFiles'))->where(['ReviewRoundFiles.submission_id' => $article->submission_id, 'SubmissionFiles.file_type' => 'application/pdf']);
            $ojs_file = json_decode(json_encode($ojs_files), true);
            $submissions_comments = $this->SubmissionComments->find('all')->where(['submission_id' => $article->submission_id, 'viewable' => 1]);
            $submission_comment = json_decode(json_encode($submissions_comments), true);
            $reviewers = $this->ReviewAssignments->find()->where(['submission_id' => $article->submission_id])->extract('reviewer_id');
            $reviewer = json_decode(json_encode($reviewers), true);
        }
        $users_ojs_first = $this->UserSettings->find('all')->where(['setting_name' => 'givenName']);
        $users_ojs_last = $this->UserSettings->find('all')->where(['setting_name' => 'familyName']);
        $user_ojs_first = json_decode(json_encode($users_ojs_first), true);
        $user_ojs_last = json_decode(json_encode($users_ojs_last), true);
        $prefers = $this->RequestHandler->prefers();

        if ($prefers == 'pdf') {
            return $this->getResponse()
                ->withFile('webroot/pubs/' . $article->article_type . '/' . $article->pdf_link);
        }
        
        $this->set('article', $article);
        if (!empty($article->submission_id)) {
            $this->set(compact('user_ojs_first', 'user_ojs_last', 'submission_comment', 'reviewer', 'ojs_file'));
        } else {
            $this->set(compact('user_ojs_first', 'user_ojs_last'));
        }
        $this->set('_serialize', 'article');
    }

    public function image($id, $name)
    {
        return $this->getResponse()->withFile('webroot/pubs/' . $id . '/images/' . $name);
    }
}
