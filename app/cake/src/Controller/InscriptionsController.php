<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Datasource\ArrayQuery;
use Cake\Http\Exception\NotAcceptableException;
use Cake\Http\Exception\RecordNotFoundException;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Inscriptions Controller
 *
 * @property \App\Model\Table\InscriptionsTable $Inscriptions
 *
 * @method \App\Model\Entity\Inscription[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InscriptionsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GranularAccess');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('LinkedData');
        $this->loadComponent('Inscription');
        $this->loadModel('Artifacts');
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Artifacts',
                'Artifacts.Dates',
                'Artifacts.Languages',
                'Artifacts.Composites' => [
                    'conditions' => ['Composites.is_public' => true]
                ]
            ],
            'conditions' => [
                'Artifacts.is_public' => true,
                'Artifacts.is_atf_public' => true,
                'Inscriptions.is_latest' => true
            ]
        ];

        if ($this->GranularAccess->canViewPrivateInscriptions()) {
            unset($this->paginate['conditions']['Artifacts.is_public']);
            unset($this->paginate['conditions']['Artifacts.is_atf_public']);
            unset($this->paginate['contain']['Artifacts.Composites']['conditions']);
        }

        $inscriptions = $this->paginate($this->Inscriptions);

        $this->set(compact('inscriptions'));
        $this->set('_serialize', 'inscriptions');
    }

    /**
     * View method
     *
     * @param string|null $id Inscription id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $inscription = $this->Inscriptions->get($id, [
            'contain' => [
                'Artifacts',
                'Artifacts.Languages',
                'Artifacts.Dates',
                'Artifacts.Composites' => [
                    'conditions' => ['Composites.is_public' => true]
                ],
                'UpdateEvents'
            ]
        ]);

        $artifact = $inscription->artifact;

        if ($this->GranularAccess->canViewPrivateInscriptions()) {
            unset($this->paginate['contain']['Artifacts.Composites']['conditions']);
        } elseif (!$artifact->is_public || !$artifact->is_atf_public) {
            throw new UnauthorizedException();
        }

        if (!$this->GranularAccess->canAccessEdits($inscription->update_event)) {
            throw new UnauthorizedException();
        }

        $type = $this->Inscription->getPreferredType($inscription);
        if (empty($type)) {
            throw new NotAcceptableException();
        }
        $this->RequestHandler->renderAs($this, $type);

        $this->set('inscription', $inscription);
        $this->set('_serialize', 'inscription');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        $canSubmitEdits = $this->GranularAccess->canSubmitEdits();
        $session = $this->getRequest()->getSession();
        $uploadErrors = 'Inscriptions.errors';
        $pendingUploads = 'Inscriptions.uploads';

        if (!$this->request->is('post')) {
            if ($session->check($pendingUploads)) {
                $inscriptions = $session->read($pendingUploads);

                if ($session->check($uploadErrors)) {
                    $errors = $session->read($uploadErrors);
                    return $this->setAction('errors', $inscriptions, $errors);
                }

                return $this->setAction('confirm', $inscriptions, $canSubmitEdits);
            }

            return;
        }

        $file = $this->request->getData('atf');

        // Overwrite with empty file ("cancel")
        if (is_null($file)) {
            $session->write($pendingUploads, []);
            $session->write($uploadErrors, []);
            return;
        }

        // Parse ATF file
        $handle = $file->getStream()->detach();
        $atf = stream_get_contents($handle);
        $records = preg_split('/(?<=\n)(?=&)/', $atf);

        $inscriptions = [];
        $errors = [];
        $hasErrors = false;

        foreach ($records as $record) {
            try {
                $inscription = $this->Inscriptions->newEntityFromAtf(trim($record));

                // Check whether user has access to inscription
                if ($inscription->has('artifact_id')) {
                    $artifact = $this->Artifacts->get($inscription->artifact_id);

                    if (!$artifact->is_public && !$this->GranularAccess->canViewPrivateArtifact()) {
                        throw new RecordNotFoundException($inscription->artifact_id);
                    }

                    if (!$artifact->is_atf_public && !$this->GranularAccess->canViewPrivateInscriptions()) {
                        throw new RecordNotFoundException($inscription->artifact_id);
                    }

                    $artifact = null;
                }

                $inscriptions[] = $inscription;
            } catch (RecordNotFoundException $error) {
                $inscription = $this->Inscriptions->newEmptyEntity();
                $inscription->setError('artifact_id', __('Record {0} not found in table "artifacts"', $error->getMessage()));
                $inscriptions[] = $inscription;
                $hasErrors = true;
            } catch (\Throwable $error) {
                $errors[] = $error->getMessage();
                $hasErrors = true;
            }
        }

        // Store updates in sessions
        $session->write($pendingUploads, $inscriptions);

        // Redirect to error page if necessary.
        if ($hasErrors) {
            $session->write($uploadErrors, $errors);
            return $this->setAction('errors', $inscriptions, $errors);
        } else {
            $session->delete($uploadErrors);
        }

        return $this->setAction('confirm', $inscriptions, $canSubmitEdits);
    }

    /**
     * @param \App\Model\Entity\Inscriptions[] $inscriptions
     * @return \Cake\Http\Response|null
     */
    public function confirm(array $inscriptions, bool $canSubmitEdits)
    {
        $inscriptions = $this->paginate(new ArrayQuery($inscriptions));

        foreach ($inscriptions as $inscription) {
            if ($inscription->has('artifact_id')) {
                $inscription->artifact = $this->Artifacts->get($inscription->artifact_id, ['contain' => ['Inscriptions']]);
            }
        }

        $this->set('inscriptions', $inscriptions);
        $this->set('canSubmitEdits', $canSubmitEdits);
    }

    /**
     * @param \App\Model\Entity\Inscriptions[] $inscriptions
     * @param string[] $errors
     * @return \Cake\Http\Response|null
     */
    public function errors(array $inscriptions, array $errors)
    {
        $inscriptions = array_filter($inscriptions, function ($inscription) {
            return $inscription->hasErrors();
        });
        $inscriptions = $this->paginate(new ArrayQuery($inscriptions));

        $this->set('inscriptions', $inscriptions);
        $this->set('errors', $errors);
    }
}
