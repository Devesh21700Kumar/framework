<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AuthorsOjsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AuthorsOjsTable Test Case
 */
class AuthorsOjsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AuthorsOjsTable
     */
    protected $AuthorsOjs;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.AuthorsOjs',
        'app.Publications',
        'app.Submissions',
        'app.UserGroups',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('AuthorsOjs') ? [] : ['className' => AuthorsOjsTable::class];
        $this->AuthorsOjs = $this->getTableLocator()->get('AuthorsOjs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->AuthorsOjs);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\AuthorsOjsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\AuthorsOjsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     * @uses \App\Model\Table\AuthorsOjsTable::defaultConnectionName()
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
