<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AbbreviationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AbbreviationsTable Test Case
 */
class AbbreviationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AbbreviationsTable
     */
    public $Abbreviations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.abbreviations',
        'app.publications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Abbreviations') ? [] : ['className' => AbbreviationsTable::class];
        $this->Abbreviations = TableRegistry::getTableLocator()->get('Abbreviations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Abbreviations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
