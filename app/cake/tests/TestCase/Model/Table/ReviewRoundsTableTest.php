<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReviewRoundsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReviewRoundsTable Test Case
 */
class ReviewRoundsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ReviewRoundsTable
     */
    protected $ReviewRounds;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ReviewRounds',
        'app.Submissions',
        'app.Stages',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ReviewRounds') ? [] : ['className' => ReviewRoundsTable::class];
        $this->ReviewRounds = $this->getTableLocator()->get('ReviewRounds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ReviewRounds);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ReviewRoundsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ReviewRoundsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     * @uses \App\Model\Table\ReviewRoundsTable::defaultConnectionName()
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
