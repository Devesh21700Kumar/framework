<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PublicationSettingsFixture
 */
class PublicationSettingsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'publication_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'locale' => ['type' => 'string', 'length' => 14, 'null' => false, 'default' => '', 'collate' => 'utf8mb4_unicode_520_ci', 'comment' => '', 'precision' => null],
        'setting_name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_unicode_520_ci', 'comment' => '', 'precision' => null],
        'setting_value' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_unicode_520_ci', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'publication_settings_publication_id' => ['type' => 'index', 'columns' => ['publication_id'], 'length' => []],
            'publication_settings_name_value' => ['type' => 'index', 'columns' => ['setting_name', 'setting_value'], 'length' => ['setting_name' => '50', 'setting_value' => '150']],
        ],
        '_constraints' => [
            'publication_settings_pkey' => ['type' => 'unique', 'columns' => ['publication_id', 'locale', 'setting_name'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_unicode_520_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'publication_id' => 1,
                'locale' => 'Lorem ipsum ',
                'setting_name' => 'Lorem ipsum dolor sit amet',
                'setting_value' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            ],
        ];
        parent::init();
    }
}
