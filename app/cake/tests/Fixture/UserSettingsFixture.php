<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UserSettingsFixture
 */
class UserSettingsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'user_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'locale' => ['type' => 'string', 'length' => 14, 'null' => false, 'default' => '', 'collate' => 'utf8mb4_unicode_520_ci', 'comment' => '', 'precision' => null],
        'setting_name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_unicode_520_ci', 'comment' => '', 'precision' => null],
        'assoc_type' => ['type' => 'biginteger', 'length' => null, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'assoc_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'setting_value' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_unicode_520_ci', 'comment' => '', 'precision' => null],
        'setting_type' => ['type' => 'string', 'length' => 6, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_unicode_520_ci', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'user_settings_user_id' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'user_settings_locale_setting_name_index' => ['type' => 'index', 'columns' => ['setting_name', 'locale'], 'length' => []],
        ],
        '_constraints' => [
            'user_settings_pkey' => ['type' => 'unique', 'columns' => ['user_id', 'locale', 'setting_name', 'assoc_type', 'assoc_id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_unicode_520_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'user_id' => 1,
                'locale' => 'Lorem ipsum ',
                'setting_name' => 'Lorem ipsum dolor sit amet',
                'assoc_type' => 1,
                'assoc_id' => 1,
                'setting_value' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'setting_type' => 'Lore',
            ],
        ];
        parent::init();
    }
}
