# Run the phpunit testing at the testing state.

# This requires the composer install.
# Generate the vendor files.
composer install

# Updated the required packages.
composer update

# Tun tests.
vendor/bin/phpunit